The GIMP Documentation Project
==============================

GIMP Help is Open Source Documentation written by a group of GIMP users and
developers. It is designed to assist both the new and more advanced user in
the use of GIMP. It is available in SGML format which can then be converted
to another form such as HTML, PDF or PostScript as desired by anyone 
using Open Source tools such as Jade. The user is welcome to print out this
documentation (it might kill a few trees, but sometimes it helps) and use
it as a general manual to the usage of the program.

1. Why SGML?
============

There are several reasons why the GIMP Documentation Project team has
chosen to use DocBook 4.1 SGML instead of the HTML which had previously
been used for the help system. The biggest of these reasons is flexibility.
DocBook SGML documents can be quickly converted to numerous other formats,
including HTML and printable formats.

Because the tagging system in SGML is content based, decisions on
appearance can be made later. This means that if in a month we decide that
menu items should no longer be displayed in bold in HTML documents and
instead in green, we merely make a new stylesheet and reconvert the files.
This saves the extensive work of going through every file and making the
desired change. It will also make it easier to use any new developments.

DocBook is also used in many other Linux documentation projects and even by
book publishers. So although many of us are new to DocBook, we can find
support in the documentation and authors of these other projects (such as
the channel #docs or irc.gimp.org which has been of much assistance
already).

We have chosen to continue with the SGML version at the present time, but
are coding in a manner which will make a later conversion to XML much
simpler. Basically all tags are written in lower case and closing tags are
always used.

2. Helping Out
==============

The Help System isn't perfect, but it's free and we're trying. If you find
any errors or omissions, please report them to the GIMP developers.
Contributions to the project are welcome.


The GIMP Documentation Team

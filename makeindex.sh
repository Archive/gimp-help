#!/bin/sh

dir=`basename \`pwd\``
subdirs=`find . -maxdepth 1 -type d`
local=`pwd | sed -e 's/^.*\/help\/C\\(.*\\)/\\1/'`

#
# Create an empty SGML file 
#

cat << EOF > index.sgml
<!doctype chapter public "-//OASIS//DTD DocBook V4.1//EN"[

EOF

#
# Create entities for all files in the directory.
#

for file in *.sgml 
do
  if [ $file != index.sgml ]; then
    name=`basename $file .sgml`
    echo "<!entity $name SYSTEM \"$file\">" >> index.sgml
  fi
done

cat << EOF >> index.sgml

]>

EOF

#
# Create a chapter with a title from the directory
#

echo "<chapter id=\"$dir-index\">" >> index.sgml
echo "  <title> Index for $dir </title>" >> index.sgml

for file in *.sgml 
do
  if [ $file != index.sgml ]; then
    name=`basename $file .sgml`
    echo "&$name;" >> index.sgml
  fi
done

#
# Create a list with indeces...
#

#set $subdirs
#
#if [ "x$3" != "x" ];
#  then
#  echo "      <para>Subtopics available:</para>" >> index.sgml
#  for dir in $subdirs 
#  do
#    if [ $dir != "." ]; 
#    then
#      if [ $dir != "./CVS" ];
#      then
#        dirbase = `basename $dir`
#        echo "    <para>" >> index.sgml      
#	echo "      <link linkend=\"$dirbase/index.sgml\">$dirbase</link>" >> index.sgml
#        echo "    </para>" >> index.sgml      
#      fi
#    fi
#  done
#fi

#
# Index sgml files in this directory.
#

#echo "  <para>Topics in this directory:</para>" >> index.sgml

#for file in *.sgml 
#do
#  if [ $file != index.sgml ]; then
#    name=`basename $file .sgml`
#    topic=`echo $name | tr "-" " "`
#    echo "    <para>" >> index.sgml      
#    echo "      <link linkend=\"$name\">$topic</link>" >> index.sgml
#    echo "    </para>" >> index.sgml      
#    fi
#done

cat << EOF >> index.sgml
</chapter>
EOF

#!/usr/bin/python

from sgmllib import SGMLParser 
import regex
import string
import sys
import types

class myparser (SGMLParser):
  outfile = sys.stdout 
  file_entities = {}
  char_entities = {'amp':'&',
		   'mdash':'--',
		   'gt':'>',
		   'percnt':'%',
		   'lt':'<'}
  
  def __init__ (self, filename, outfile = sys.stdout):
    SGMLParser.__init__ (self)
    if (self.file_entities == {}): 
      self.preparse (filename)
    self.outfile = outfile
    self.file = open (filename, 'r')
    self.feed (self.file.read())
    self.file.close ()
   
  #def handle_comment (self, comment):
  #  self.outfile.write ('<!--' + comment + '-->')

  def handle_data (self, data):
    self.outfile.write (data)

  #def handle_charref (self, ref):
   # print 'ref: ' + ref
  
  #def handle_pi (self, pi):
    #print 'Processing instruction: ' + pi 
 
  def unknown_starttag (self, tag, attrs):
    print "Unknown tag " + tag
  
  def handle_entityref (self, ref):
    if (self.file_entities.has_key(ref)):
      subparser = myparser (self.file_entities[ref], self.outfile) 
    elif (self.char_entities.has_key(ref)):
      self.outfile.write (self.char_entities[ref])
    else:
      print "Unknown entity " + ref

  def start_unknown (self, attrs):
    print attrs 
  
  def start_para (self, attrs):
    self.outfile.write ('<para>')
  
  def end_para (self):
    self.outfile.write ('</para>')
  
  def start_sect1 (self, attrs):
    self.outfile.write ('<sect1>')
  
  def end_sect1 (self):
    self.outfile.write ('</sect1>')
  
  def start_sect2 (self, attrs):
    self.outfile.write ('<sect2>')
  
  def end_sect2 (self):
    self.outfile.write ('</sect2>')
  
  def start_sect3 (self, attrs):
    self.outfile.write ('<sect3>')

  def end_sect3 (self):
    self.outfile.write ('</sect3>')
  
  def start_chapter (self, attrs):
    self.outfile.write ('<chapter>')

  def end_chapter (self):
    self.outfile.write ('</chapter>')

  def start_book (self, attrs):
    self.outfile.write ('<book>')

  def end_book (self):
    self.outfile.write ('</book>')
  
  def start_bookinfo (self, attrs):
    self.outfile.write ('<bookinfo>')

  def end_bookinfo (self):
    self.outfile.write ('</bookinfo>')
  
  def start_title (self, attrs):
    self.outfile.write ('<title>')

  def end_title (self):
    self.outfile.write ('</title>')
  
  def start_copyright (self, attrs):
    self.outfile.write ('<copyright>')

  def end_copyright (self):
    self.outfile.write ('</copyright>')
  
  def start_abbrev (self, attrs):
    self.outfile.write ('<abbrev>')

  def end_abbrev (self):
    self.outfile.write ('</abbrev>')
  
  def start_acronym (self, attrs):
    self.outfile.write ('<acronym>')

  def end_acronym (self):
    self.outfile.write ('</acronym>')
  
  def start_application (self, attrs):
    self.outfile.write ('<application>')

  def end_application (self):
    self.outfile.write ('</application>')
  
  def start_year (self, attrs):
    self.outfile.write ('<year>')

  def end_year (self):
    self.outfile.write ('</year>')
  
  def start_literallayout (self, attrs):
    self.outfile.write ('<literallayout>')

  def end_literallayout (self):
    self.outfile.write ('</literallayout>')
  
  def start_optional (self, attrs):
    self.outfile.write ('<optional>')

  def end_optional (self):
    self.outfile.write ('</optional>')
  
  def start_appendix (self, attrs):
    self.outfile.write ('<appendix>')

  def end_appendix (self):
    self.outfile.write ('</appendix>')
  
  def start_superscript (self, attrs):
    self.outfile.write ('<superscript>')

  def end_superscript (self):
    self.outfile.write ('</superscript>')
  
  def start_sgmltag (self, attrs):
    self.outfile.write ('<sgmltag>')

  def end_sgmltag (self):
    self.outfile.write ('</sgmltag>')
  
  def start_trademark (self, attrs):
    self.outfile.write ('<trademark>')

  def end_trademark (self):
    self.outfile.write ('</trademark>')
  
  def start_option (self, attrs):
    self.outfile.write ('<option>')

  def end_option (self):
    self.outfile.write ('</option>')
  
  def start_userinput (self, attrs):
    self.outfile.write ('<userinput>')

  def end_userinput (self):
    self.outfile.write ('</userinput>')
  
  def start_arg (self, attrs):
    self.outfile.write ('<arg>')

  def end_arg (self):
    self.outfile.write ('</arg>')
  
  def start_cmdsynopsis (self, attrs):
    self.outfile.write ('<cmdsynopsis>')

  def end_cmdsynopsis (self):
    self.outfile.write ('</cmdsynopsis>')
  
  def start_computeroutput (self, attrs):
    self.outfile.write ('<computeroutput>')

  def end_computeroutput (self):
    self.outfile.write ('</computeroutput>')
  
  def start_tbody (self, attrs):
    self.outfile.write ('<tbody>')

  def end_tbody (self):
    self.outfile.write ('</tbody>')
  
  def start_thead (self, attrs):
    self.outfile.write ('<thead>')

  def end_thead (self):
    self.outfile.write ('</thead>')
  
  def start_tgroup (self, attrs):
    self.outfile.write ('<tgroup>')

  def end_tgroup (self):
    self.outfile.write ('</tgroup>')
  
  def start_hardware (self, attrs):
    self.outfile.write ('<hardware>')

  def end_hardware (self):
    self.outfile.write ('</hardware>')
  
  def start_accel (self, attrs):
    self.outfile.write ('<accel>')

  def end_accel (self):
    self.outfile.write ('</accel>')
  
  def start_emphasis (self, attrs):
    self.outfile.write ('<emphasis>')

  def end_emphasis (self):
    self.outfile.write ('</emphasis>')
  
  def start_email (self, attrs):
    self.outfile.write ('<email>')

  def end_email (self):
    self.outfile.write ('</email>')
  
  def start_tip (self, attrs):
    self.outfile.write ('<tip>')

  def end_tip (self):
    self.outfile.write ('</tip>')
  
  def start_parameter (self, attrs):
    self.outfile.write ('<parameter>')

  def end_parameter (self):
    self.outfile.write ('</parameter>')
  
  def start_keysym (self, attrs):
    self.outfile.write ('<keysym>')

  def end_keysym (self):
    self.outfile.write ('</keysym>')
  
  def start_table (self, attrs):
    self.outfile.write ('<table>')

  def end_table (self):
    self.outfile.write ('</table>')
  
  def start_command (self, attrs):
    self.outfile.write ('<command>')

  def end_command (self):
    self.outfile.write ('</command>')
  
  def start_ulink (self, attrs):
    self.outfile.write ('<ulink>')

  def end_ulink (self):
    self.outfile.write ('</ulink>')
  
  def start_colspec (self, attrs):
    self.outfile.write ('<colspec>')

  def end_colspec (self):
    self.outfile.write ('</colspec>')
  
  def start_productname (self, attrs):
    self.outfile.write ('<productname>')

  def end_productname (self):
    self.outfile.write ('</productname>')
  
  def start_footnote (self, attrs):
    self.outfile.write ('<footnote>')

  def end_footnote (self):
    self.outfile.write ('</footnote>')
  
  def start_mediaobject (self, attrs):
    self.outfile.write ('<mediaobject>')

  def end_mediaobject (self):
    self.outfile.write ('</mediaobject>')
  
  def start_replaceable (self, attrs):
    self.outfile.write ('<replaceable>')

  def end_replaceable (self):
    self.outfile.write ('</replaceable>')
  
  def start_figure (self, attrs):
    self.outfile.write ('<figure>')

  def end_figure (self):
    self.outfile.write ('</figure>')
  
  def start_guiicon (self, attrs):
    self.outfile.write ('<guiicon>')

  def end_guiicon (self):
    self.outfile.write ('</guiicon>')
  
  def start_glossary (self, attrs):
    self.outfile.write ('<glossary>')

  def end_glossary (self):
    self.outfile.write ('</glossary>')
  
  def start_glosssee (self, attrs):
    self.outfile.write ('<glosssee>')

  def end_glosssee (self):
    self.outfile.write ('</glosssee>')
  
  def start_glossdef (self, attrs):
    self.outfile.write ('<glossdef>')

  def end_glossdef (self):
    self.outfile.write ('</glossdef>')
  
  def start_glossdiv (self, attrs):
    self.outfile.write ('<glossdiv>')

  def end_glossdiv (self):
    self.outfile.write ('</glossdiv>')
  
  def start_glossentry (self, attrs):
    self.outfile.write ('<glossentry>')

  def end_glossentry (self):
    self.outfile.write ('</glossentry>')
  
  def start_glossterm (self, attrs):
    self.outfile.write ('<glossterm>')

  def end_glossterm (self):
    self.outfile.write ('</glossterm>')
  
  def start_xref (self, attrs):
    self.outfile.write ('<xref>')

  def end_xref (self):
    self.outfile.write ('</xref>')
  
  def start_note (self, attrs):
    self.outfile.write ('<note>')

  def end_note (self):
    self.outfile.write ('</note>')
  
  def start_filename (self, attrs):
    self.outfile.write ('<filename>')

  def end_filename (self):
    self.outfile.write ('</filename>')
  
  def start_warning (self, attrs):
    self.outfile.write ('<warning>')

  def end_warning (self):
    self.outfile.write ('</warning>')
  
  def start_phrase (self, attrs):
    self.outfile.write ('<phrase>')

  def end_phrase (self):
    self.outfile.write ('</phrase>')
  
  def start_literal (self, attrs):
    self.outfile.write ('<literal>')

  def end_literal (self):
    self.outfile.write ('</literal>')
  
  def start_textobject (self, attrs):
    self.outfile.write ('<textobject>')

  def end_textobject (self):
    self.outfile.write ('</textobject>')
  
  def start_inlinemediaobject (self, attrs):
    self.outfile.write ('<inlinemediaobject>')

  def end_inlinemediaobject (self):
    self.outfile.write ('</inlinemediaobject>')
  
  def start_imagedata (self, attrs):
    self.outfile.write ('<imagedata>')

  def end_imagedata (self):
    self.outfile.write ('</imagedata>')
  
  def start_imageobject (self, attrs):
    self.outfile.write ('<imageobject>')

  def end_imageobject (self):
    self.outfile.write ('</imageobject>')
  
  def start_guimenu (self, attrs):
    self.outfile.write ('<guimenu>')

  def end_guimenu (self):
    self.outfile.write ('</guimenu>')
  
  def start_itemizedlist (self, attrs):
    self.outfile.write ('<itemizedlist>')

  def end_itemizedlist (self):
    self.outfile.write ('</itemizedlist>')
  
  def start_variablelist (self, attrs):
    self.outfile.write ('<variablelist>')

  def end_variablelist (self):
    self.outfile.write ('</variablelist>')
  
  def start_quote (self, attrs):
    self.outfile.write ('<quote>')

  def end_quote (self):
    self.outfile.write ('</quote>')
  
  def start_varlistentry (self, attrs):
    self.outfile.write ('<varlistentry>')

  def end_varlistentry (self):
    self.outfile.write ('</varlistentry>')
  
  def start_bridgehead (self, attrs):
    self.outfile.write ('<bridgehead>')

  def end_bridgehead (self):
    self.outfile.write ('</bridgehead>')
  
  def start_link (self, attrs):
    self.outfile.write ('<link>')

  def end_link (self):
    self.outfile.write ('</link>')
  
  def start_term (self, attrs):
    self.outfile.write ('<term>')

  def end_term (self):
    self.outfile.write ('</term>')
  
  def start_guisubmenu (self, attrs):
    self.outfile.write ('<guisubmenu>')

  def end_guisubmenu (self):
    self.outfile.write ('</guisubmenu>')
  
  def start_guimenuitem (self, attrs):
    self.outfile.write ('<guimenuitem>')

  def end_guimenuitem (self):
    self.outfile.write ('</guimenuitem>')
  
  def start_row (self, attrs):
    self.outfile.write ('<row>')

  def end_row (self):
    self.outfile.write ('</row>')
  
  def start_mousebutton (self, attrs):
    self.outfile.write ('<mousebutton>')

  def end_mousebutton (self):
    self.outfile.write ('</mousebutton>')
  
  def start_keycombo (self, attrs):
    self.outfile.write ('<keycombo>')

  def end_keycombo (self):
    self.outfile.write ('</keycombo>')
  
  def start_keycap (self, attrs):
    self.outfile.write ('<keycap>')

  def end_keycap (self):
    self.outfile.write ('</keycap>')
  
  def start_guilabel (self, attrs):
    self.outfile.write ('<guilabel>')

  def end_guilabel (self):
    self.outfile.write ('</guilabel>')
  
  def start_guibutton (self, attrs):
    self.outfile.write ('<guibutton>')

  def end_guibutton (self):
    self.outfile.write ('</guibutton>')
  
  def start_entry (self, attrs):
    self.outfile.write ('<entry>')

  def end_entry (self):
    self.outfile.write ('</entry>')
  
  def start_listitem (self, attrs):
    self.outfile.write ('<listitem>')

  def end_listitem (self):
    self.outfile.write ('</listitem>')
  
  def start_holder (self, attrs):
    self.outfile.write ('<holder>')

  def end_holder (self):
    self.outfile.write ('</holder>')
  
  def start_legalnotice (self, attrs):
    self.outfile.write ('<legalnotice>')

  def end_legalnotice (self):
    self.outfile.write ('</legalnotice>')
  
  def start_indexterm (self, attrs):
    self.outfile.write ('<indexterm>')

  def end_indexterm (self):
    self.outfile.write ('</indexterm>')
  
  def start_primary (self, attrs):
    self.outfile.write ('<primary>')

  def end_primary (self):
    self.outfile.write ('</primary>')
  
  def start_secondary (self, attrs):
    self.outfile.write ('<secondary>')

  def end_secondary (self):
    self.outfile.write ('</secondary>')
  
  def preparse (self, filename):
    __file = open (filename, 'r')
    entitydef = regex.compile ('<!entity +\([^ ]+\) +SYSTEM +\"\([^ ]+\)\">')
    
    while 1:
      line = __file.readline ()
      if (not line):
	break
      if (entitydef.match (line) >= 0):
	self.file_entities[entitydef.group (1)] = entitydef.group (2)
      
    __file.close ()


outfile = open ('gimp.xml', 'w')
myparser ('gimp.sgml', outfile)
outfile.close ()

#!/usr/bin/python

from sgmllib import SGMLParser 
import regex
import string
import sys
import types

class myparser (SGMLParser):
 
  # Add ampersand to middle items of a table
  add_ampersand = 0

  in_legalnotice = 0
  in_variablelist = 0
  in_variablelist_item = 0
  in_table = 0
  in_title = 0
  in_glossary = 0
  in_keycombo = 0
  do_glossary_title = 0
  do_booktitle = 0
  
  anchor_title = ''
  
  outfile = sys.stdout 
  file_entities = {}
  char_entities = {'amp':'\&',
		   'mdash':'{ }--{ }',
		   'gt':'>',
		   'lt':'<',
		   'percnt':'\%'}
  
  def __init__ (self, filename, outfile = sys.stdout):
    SGMLParser.__init__ (self)
    self.outfile = outfile
    if (self.file_entities == {}): 
      self.make_preamble ()
      self.preparse (filename)
    self.file = open (filename, 'r')
    self.rubbish = regex.compile ('<!doctype +')
    self.rubbish2 = regex.compile (']>')
    self.feed (self.file.read())
    self.file.close ()
   
  #def handle_comment (self, comment):
  #  self.outfile.write ('<!--' + comment + '-->')

  def handle_data (self, data):
    if (self.rubbish.match (data) == -1):
      if (self.rubbish2.search (data) == -1):
	self.outfile.write (
	data.replace ('\\', '\\\\').
	replace ('$','\$').
	replace ('_','\_').
	replace ('~','\~{ }').
	replace ('#','\#').
	rstrip())
	

  #def handle_charref (self, ref):
   # print 'ref: ' + ref
  
  #def handle_pi (self, pi):
    #print 'Processing instruction: ' + pi 

  def unknown_entityref(self, name):
    print "reference to unknown entity `&%s;'" % name)

 
  def unknown_starttag (self, tag, attrs):
    print "Unknown tag " + tag
  
  def handle_entityref (self, ref):
    if (self.file_entities.has_key(ref)):
      subparser = myparser (self.file_entities[ref], self.outfile) 
    elif (self.char_entities.has_key(ref)):
      self.outfile.write (self.char_entities[ref])
    else:
      print "Unknown entity " + ref

  def start_unknown (self, attrs):
    print attrs 
  
  def start_para (self, attrs):
    pass 
  
  def end_para (self):
    self.outfile.write ('\n')
  
  def start_simplesect (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\paragraph')
  
  def end_simplesect (self):
    self.outfile.write ('\n\\vskip\\lineskip')

  def start_sect1 (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\section')
  
  def end_sect1 (self):
    self.outfile.write ('\n')
  
  def start_sect2 (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\subsection')
  
  def end_sect2 (self):
    self.outfile.write ('\n')
  
  def start_sect3 (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\subsubsection')

  def end_sect3 (self):
    self.outfile.write ('\n')
  
  def start_chapter (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\chapter')

  def end_chapter (self):
    self.outfile.write ('\n')

  def start_book (self, attrs):
    self.outfile.write ('\\makeindex\n')    
    self.outfile.write ('\\begin{document}\n')    
    self.do_booktitle = 1

  def end_book (self):
    self.outfile.write ('\\printindex\n')    
    self.outfile.write ('\\end{document}\n')
  
  def start_bookinfo (self, attrs):
    pass

  def end_bookinfo (self):
    self.outfile.write ('\\tableofcontents\n')
  
  def start_title (self, attrs):
    self.in_title = 1
    if (self.do_glossary_title):
      self.outfile.write ('\\chapter{')
    elif (self.do_booktitle):
      self.outfile.write ('\\title{')
    elif (self.in_table):
      self.outfile.write ('{\\large ')
    elif (self.in_legalnotice):
      self.outfile.write ('{\\Large ')
    else:
      if (not self.in_variablelist):
	if (not self.in_table):
	  self.outfile.write ('{')

  def end_title (self):
    self.in_title = 0
    if (self.in_variablelist):
      self.outfile.write ('\\begin{description}\n')
      self.in_variablelist = 0
    elif (self.in_table):
      self.outfile.write ('}\n\n')
      self.in_table = 0
    elif (self.in_legalnotice):
      self.outfile.write ('\n}\n')
      self.in_legalnotice = 0
    elif (self.do_glossary_title):
      self.outfile.write ('}\n')
      self.do_glossary_title = 0
    elif (self.do_booktitle):
      self.outfile.write ('}\n')
      self.do_booktitle = 0
    else:
      self.outfile.write ('}\n')
      if (self.anchor_title):
	self.outfile.write ('\\label{' + self.anchor_title + '}\n')
        self.anchor_title = '' 
      else:
        self.outfile.write ('\n')

  def start_copyright (self, attrs):
    pass

  def end_copyright (self):
    self.outfile.write ('\\maketitle\n')
  
  def start_abbrev (self, attrs):
    self.outfile.write (' ')

  def end_abbrev (self):
    pass
  
  def start_acronym (self, attrs):
    if (not self.in_title):
      self.outfile.write (' {\\sc ')
    else: 
      self.outfile.write (' ')

  def end_acronym (self):
    if (not self.in_title):
      self.outfile.write ('}')
  
  def start_application (self, attrs):
    if (not self.in_title):
      self.outfile.write (' {\\sc ')
    else: 
      self.outfile.write (' ')

  def end_application (self):
    if (not self.in_title):
      self.outfile.write ('}')
  
  def start_year (self, attrs):
    self.outfile.write ('\date{')

  def end_year (self):
    self.outfile.write ('}\n')
  
  def start_literallayout (self, attrs):
    self.outfile.write (' \\begin{verbatim}\n')

  def end_literallayout (self):
    self.outfile.write (' \\end{verbatim}\n')

  def start_optional (self, attrs):
    #self.outfile.write ('<optional>')
    pass

  def end_optional (self):
    #self.outfile.write ('</optional>')
    pass
  
  def start_appendix (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\\chapter')

  def end_appendix (self):
    self.outfile.write ('\n')
  
  def start_superscript (self, attrs):
    #self.outfile.write ('<superscript>')
    pass

  def end_superscript (self):
    #self.outfile.write ('</superscript>')
    pass
  
  def start_sgmltag (self, attrs):
    #self.outfile.write ('<sgmltag>')
    pass

  def end_sgmltag (self):
    #self.outfile.write ('</sgmltag>')
    pass
  
  def start_trademark (self, attrs):
    #self.outfile.write ('<trademark>')
    pass

  def end_trademark (self):
    #self.outfile.write ('</trademark>')
    pass
  
  def start_option (self, attrs):
    #self.outfile.write ('<option>')
    pass

  def end_option (self):
    #self.outfile.write ('</option>')
    pass
  
  def start_userinput (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_userinput (self):
    self.outfile.write ('}')
  
  def start_arg (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_arg (self):
    self.outfile.write ('}')
  
  def start_cmdsynopsis (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_cmdsynopsis (self):
    self.outfile.write ('}')
  
  def start_computeroutput (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_computeroutput (self):
    self.outfile.write ('}')
  
  def start_tbody (self, attrs):
    #self.outfile.write ('<tbody>')
    pass

  def end_tbody (self):
    #self.outfile.write ('</tbody>')
    pass
  
  def start_thead (self, attrs):
    pass

  def end_thead (self):
    self.outfile.write ('\\hline\n')
  
  def start_tgroup (self, attrs):
    align = 'l'
    for i in range (len(attrs)):
      if (attrs[i][0] == 'cols'):
	cols = int (attrs[i][1])
      elif (attrs[i][0] == 'colsep'):	
	colsep = attrs[i][1]
      elif (attrs[i][0] == 'align'):	
	align = attrs[i][1][:1]
      elif (attrs[i][0] == 'rowsep'):	
	rowsep = attrs[i][1]

    self.outfile.write ('\\hfill\\begin{tabular}{')

    for i in range (cols):
      self.outfile.write (align)
    
    self.outfile.write ('}\n')

  def end_tgroup (self):
    pass
  
  def start_hardware (self, attrs):
    pass

  def end_hardware (self):
    pass
  
  def start_accel (self, attrs):
    pass

  def end_accel (self):
    pass
  
  def start_emphasis (self, attrs):
    self.outfile.write ('{\\emph ')

  def end_emphasis (self):
    self.outfile.write ('}')
  
  def start_email (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_email (self):
    self.outfile.write ('}')
  
  def start_tip (self, attrs):
    self.outfile.write ('\\infoicon{stylesheet-images/print/tip.png}{') 

  def end_tip (self):
    self.outfile.write ('}')
  
  def start_parameter (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_parameter (self):
    self.outfile.write ('}')
  
  def start_keysym (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_keysym (self):
    self.outfile.write ('}')
  
  def start_table (self, attrs):
    self.in_table = 1
    self.outfile.write ('\\begin{center}\n' +
			'\\fbox{\\begin{minipage}{0.85\\linewidth}' + 
			'\\begin{center}')  

  def end_table (self):
    self.in_table = 0
    self.outfile.write ('\\end{tabular}\\end{center}' +
			'\\end{minipage}}\\end{center}\n')
  
  def start_command (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_command (self):
    self.outfile.write ('}')
  
  def start_ulink (self, attrs):
    pass

  def end_ulink (self):
    pass
  
  def start_colspec (self, attrs):
    pass

  def end_colspec (self):
    pass
  
  def start_productname (self, attrs):
    if (not self.in_title):
      self.outfile.write (' {\\sc ')
    else: 
      self.outfile.write (' ')

  def end_productname (self):
    if (not self.in_title):
      self.outfile.write ('}')

  def start_footnote (self, attrs):
    self.outfile.write ('\\footnote{')

  def end_footnote (self):
    self.outfile.write ('}')
  
  def start_mediaobject (self, attrs):
    self.outfile.write ('\\begin{center}\n')

  def end_mediaobject (self):
    self.outfile.write ('\\\\ \\end{center}\n')
  
  def start_replaceable (self, attrs):
    pass

  def end_replaceable (self):
    pass
  
  def start_figure (self, attrs):
    self.outfile.write ('\\par\\medskip\n' +
    			'\\begin{minipage}{\\linewidth}\\begin{center}\n')
    for i in range (len(attrs)):
      if (attrs[i][0] == 'id'):
       self.outfile.write ('\\label{' + attrs[i][1] + '}')

  def end_figure (self):
    self.outfile.write ('\\end{center}\\end{minipage}\\bigskip\n\\par\n')
  
  def start_guiicon (self, attrs):
    self.outfile.write (' \\emph{ ')

  def end_guiicon (self):
    self.outfile.write ('}')
  
  def start_glossary (self, attrs):
    self.do_glossary_title = 1
    self.in_glossary = 1

  def end_glossary (self):
    self.in_glossary = 0
  
  def start_glosssee (self, attrs):
    pass

  def end_glosssee (self):
    pass
  
  def start_glossdef (self, attrs):
    pass

  def end_glossdef (self):
    pass
  
  def start_glossdiv (self, attrs):
    self.outfile.write ('\n\\section* ')
    self.in_variablelist = 1	

  def end_glossdiv (self):
    self.outfile.write ('\\end{description}\n')  
  
  def start_glossentry (self, attrs):
    if (self.in_variablelist):
      self.outfile.write ('\n\\begin{description}\n')
      self.in_variablelist = 0
    self.outfile.write ('\\item')

  def end_glossentry (self):
    pass
  
  def start_glossterm (self, attrs):
    self.outfile.write ('[')

  def end_glossterm (self):
    self.outfile.write (']')
  
  def start_xref (self, attrs):
    index = attrs.pop()
    if (index[0] == 'linkend'):
      self.outfile.write (' \\pageref{'+index[1]+'}')

  def end_xref (self):
    pass

  def start_note (self, attrs):
    self.outfile.write ('\\infoicon{stylesheet-images/print/note.png}{') 

  def end_note (self):
    self.outfile.write ('}\n')
  
  def start_filename (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_filename (self):
    self.outfile.write ('}')
  
  def start_warning (self, attrs):
    self.outfile.write ('\\infoicon{stylesheet-images/print/warning.png}{\n') 

  def end_warning (self):
    self.outfile.write ('}\n')
  
  def start_phrase (self, attrs):
    self.outfile.write (' \\ignore{')

  def end_phrase (self):
    self.outfile.write ('}')
  
  def start_literal (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_literal (self):
    self.outfile.write ('}')
  
  def start_textobject (self, attrs):
    self.outfile.write (' \\ignore{')

  def end_textobject (self):
    self.outfile.write ('}')
  
  def start_inlinemediaobject (self, attrs):
    self.outfile.write ('\\raisebox{-2pt}{ ')

  def end_inlinemediaobject (self):
    self.outfile.write ('}')

  def start_imagedata (self, attrs):
    for i in range (len (attrs)):
      if (attrs[i][0] == 'fileref'):
	self.outfile.write (' \\includegraphics{' + 
	                     attrs[i][1].replace('../','') + '} ') 

  def end_imagedata (self):
    #self.outfile.write ('</imagedata>')
    pass
  
  def start_imageobject (self, attrs):
    #self.outfile.write ('<imageobject>')
    pass

  def end_imageobject (self):
    #self.outfile.write ('</imageobject>')
    pass
  
  def start_example (self, attrs):
    pass

  def end_example (self):
    pass

  def start_guimenu (self, attrs):
    self.outfile.write (' {\\tt ')

  def end_guimenu (self):
    self.outfile.write ('}')
  
  def start_itemizedlist (self, attrs):
    self.in_variablelist_item = 0
    self.outfile.write ('\\begin{itemize}\n')

  def end_itemizedlist (self):
    self.outfile.write ('\\end{itemize}\n')
  
  def start_variablelist (self, attrs):
    self.in_variablelist = 1

  def end_variablelist (self):
    self.outfile.write ('\\end{description}\n')
  
  def start_quote (self, attrs):
    self.outfile.write (' ``')

  def end_quote (self):
    self.outfile.write ('\'\'')
  
  def start_varlistentry (self, attrs):
    self.in_variablelist_item  = 1
    if (self.in_variablelist):
      self.outfile.write ('\\begin{description}\n')
      self.in_variablelist = 0
    self.outfile.write ('\\item')

  def end_varlistentry (self):
    self.in_variablelist_item  = 0  
  
  def start_bridgehead (self, attrs):
    self.make_anchor (attrs)
    self.outfile.write ('\n\\medskip\\noindent{\\bf ')

  def end_bridgehead (self):
    self.outfile.write ('}\n')  
 
  def start_link (self, attrs):
    #self.outfile.write ('<link>')
    pass

  def end_link (self):
    #self.outfile.write ('</link>')
    pass
  
  def start_term (self, attrs):
    self.outfile.write ('[')

  def end_term (self):
    self.outfile.write (']')
  
  def start_guisubmenu (self, attrs):
    if (not self.in_title):
      self.outfile.write (' {\\tt ')
    else: 
      self.outfile.write (' ')

  def end_guisubmenu (self):
    if (not self.in_title):
      self.outfile.write ('}')
  
  def start_guimenuitem (self, attrs):
    if (not self.in_title):
      self.outfile.write (' {\\tt ')
    else: 
      self.outfile.write (' ')

  def end_guimenuitem (self):
    if (not self.in_title):
      self.outfile.write ('}')
  
  def start_row (self, attrs):
    #self.outfile.write ('<row>')
    pass

  def end_row (self):
    self.add_ampersand = 0
    self.outfile.write ('\\\\\n')
  
  def start_mousebutton (self, attrs):
    self.outfile.write (' {\\emph ')

  def end_mousebutton (self):
    self.outfile.write ('}')
  
  def start_keycombo (self, attrs):
    self.in_keycombo = 1
    self.outfile.write (' ')

  def end_keycombo (self):
    self.in_keycombo = 0
    self.outfile.write (' ')
  
  def start_keycap (self, attrs):
    if (not self.in_keycombo):  
       self.outfile.write (' ')	
    self.outfile.write ('{\\ovalbox{\\raisebox{0pt}[0.8em][0.2ex]{\\bf ')

  def end_keycap (self):
    self.outfile.write ('}}}')
  
  def start_guilabel (self, attrs):
    self.outfile.write (' {\\emph ')

  def end_guilabel (self):
    self.outfile.write ('}')
  
  def start_guibutton (self, attrs):
    self.outfile.write (' \\emph{ ')

  def end_guibutton (self):
    self.outfile.write ('}')
  
  def start_entry (self, attrs):
    if self.add_ampersand:
      self.outfile.write (' & ')

  def end_entry (self):
    self.add_ampersand = 1 
  
  def start_listitem (self, attrs):
    if (not self.in_variablelist):
      if (not self.in_variablelist_item):
	self.outfile.write ('\\item ')
  
  def end_listitem (self):
   pass
 
  def start_holder (self, attrs):
    self.outfile.write ('\\author{')

  def end_holder (self):
    self.outfile.write ('}\n')
 
  def start_legalnotice (self, attrs):
    self.in_legalnotice = 1
    self.outfile.write ('\\begin{tt}\\noindent\n')

  def end_legalnotice (self):
    self.outfile.write ('\\end{tt}\n')
  
  def start_indexterm (self, attrs):
    self.outfile.write ('\\index')

  def end_indexterm (self):
    pass
  
  def start_primary (self, attrs):
    self.outfile.write ('{')

  def end_primary (self):
    self.outfile.write ('}')
  
  def start_secondary (self, attrs):
    self.outfile.write ('{')

  def end_secondary (self):
    self.outfile.write ('}')
  
  # Misc auxilliary functions
  
  def make_anchor (self, attrs):
    for i in range (len(attrs)):
      if (attrs[i][0] == 'id'):
	self.anchor_title = attrs[i][1]
  
  def preparse (self, filename):
    __file = open (filename, 'r')
    entitydef = regex.compile ('<!entity +\([^ ]+\) +SYSTEM +\"\([^ ]+\)\">')
    
    while 1:
      line = __file.readline ()
      if (not line):
	break
      if (entitydef.match (line) >= 0):
	self.file_entities[entitydef.group (1)] = entitydef.group (2)
      
    __file.close ()

  def make_preamble (self):
    # Load shitload of packages
    self.outfile.write ('\\documentclass[pdftex, 10pt, a4paper,')
    self.outfile.write (' titlepage, oneside]{book}\n')
    self.outfile.write ('\\usepackage[T1]{fontenc}\n')
    self.outfile.write ('\\usepackage{makeidx}\n')
    self.outfile.write ('\\usepackage{nameref}\n')
    self.outfile.write ('\\usepackage{textcomp}\n')
    self.outfile.write ('\\usepackage{cmbright}\n')
    self.outfile.write ('\\usepackage{pslatex}\n')
    self.outfile.write ('\\usepackage{graphicx}\n')
    self.outfile.write ('\\usepackage{fancybox}\n')
    self.outfile.write ('\\usepackage{color}\n')
    self.outfile.write ('\\usepackage[nonindentfirst]{titlesec}\n')
    self.outfile.write ('\\usepackage{tocloft}\n')
    self.outfile.write ('\\usepackage[colorlinks, ')
    self.outfile.write ('  pdftitle={GIMP User Manual},\n')
    self.outfile.write ('  linkcolor={black}]{hyperref}\n')

    # TeX output customisations

    self.outfile.write ('\\titleformat{\\chapter}[display]\n')
    self.outfile.write (' {\\normalfont\\Large\\filright }\n')
    self.outfile.write (' {\\LARGE\MakeUppercase{\\chaptername}\\thechapter}\n')
    self.outfile.write (' {0.2pc}\n')
    self.outfile.write (' {\\titlerule\n')
    self.outfile.write ('  \\vspace{0.2pc}%\n')
    self.outfile.write ('  \\Huge\\hfill}\n')

    self.outfile.write ('\\setlength{\cftsecnumwidth}{3.2em}\n')
    self.outfile.write ('\\setlength{\cftsubsecnumwidth}{4em}\n')
    self.outfile.write ('\\renewcommand{\\cftdotsep}{2}\n')
    self.outfile.write ('\\renewcommand{\\cfttoctitlefont}\n')
    self.outfile.write ('  {\\newpage\\thispagestyle{empty}\\Huge\\hfill}\n')

    # A macro for the rendering of warnings, tips and notes.
    self.outfile.write ('\\newcommand{\\infoicon}[2]{%\n')  
    self.outfile.write ('  \\par\\medskip\\ovalbox{\\begin{minipage}')
    self.outfile.write ('  {\\linewidth}\n')
    self.outfile.write ('    \\begin{center}\n')
    self.outfile.write ('      \\begin{minipage}[c]{0.12\\linewidth}\n')
    self.outfile.write ('        \\includegraphics{#1}\n')
    self.outfile.write ('      \\end{minipage}\\hfill\n')
    self.outfile.write ('      \\begin{minipage}[c]{0.85\\linewidth}\n')
    self.outfile.write ('        #2\n')
    self.outfile.write ('      \\end{minipage}\n')
    self.outfile.write ('    \\end{center}\n')
    self.outfile.write ('  \\end{minipage}\\hspace{1em}}\\par\\medskip\n')
    self.outfile.write ('  \\noindent}\n')
    self.outfile.write ('\\newcommand{\\ignore}[1]{}\n')

    # Depth of table of contents 
    self.outfile.write ('\\setcounter{tocdepth}{2}\n')  

outfile = open ('gimp-pdf.tex', 'w')

myparser ('gimp.sgml', outfile)
outfile.close ()

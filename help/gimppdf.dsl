<!--
  DSSSL stylesheet for ready-to-print output of the GIMP Documentation Project
  (C) 2001 by Daniel Egger <egger@suse.de> under the GPL.
-->

<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY docbook.dsl PUBLIC "-//Norman Walsh//DOCUMENT DocBook Print Stylesheet//EN" CDATA dsssl>
]>

<style-sheet>
  <style-specification use="docbook">
    <style-specification-body> 

      ;; Make crossreferences appear in italic style
      (define %refentry-xref-italic% #t)
      
      ;; Don't label sections in the preface
      (define %label-preface-sections% #f)
     
      ;; Which papertype are we formatting for?
      ;; For US letter use "USletter", since A4 is the default
      ;; I just document this feature here
      (define %paper-type% "A4")

      ;; Format for onesidex or duplex printing?
      (define %two-side% #t)

      ;; Use the graphic buttons
      (define %admon-graphics-path% "stylesheet-images/")
      (define %admon-graphics% #t)
      (define admon-graphic-default-extension ".png")
     
      ;; Create footnotes for misc links
      (define %footnote-ulinks% #t)

      ;; Indent all nontext by specified amount
      (define %block-start-indent% 10pt)

      ;; We'll just use PNG for the graphics
      (define %graphic-default-extension% ".png")

    </style-specification-body>
  </style-specification>
  <external-specification id="docbook" document="docbook.dsl">
</style-sheet>

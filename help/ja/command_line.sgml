<!--
<!doctype appendix public "-//OASIS//DTD DocBook V4.0//EN">
-->

<appendix id="command-line">
  <title>コマンドラインオプション</title>
  <indexterm><primary>コマンドラインオプション</primary></indexterm>
  <?dbhtml filename="command_line.html">
  
  <sect1>
    <title>GIMP</title>
    <indexterm><primary>GIMP</primary></indexterm>
    <para>
<application>GIMP</application> を起動する時、たいていは <application>GIMP</application> の項目をクリックするだけのグラフィカルなメニューでしょう。これは通常画像を読み込まずデフォルトの状態で <application>GIMP</application> を起動します。コンソールから <application>GIMP</application> を起動したり、メニューからの起動に使用するコマンドを編集するなら、これにいくつかのオプションを渡すことができます。書式は次の通りです。<command>gimp</command> <optional>options</optional> <replaceable>files ...</replaceable>
<!-- 
      When you launch <application>GIMP</application>, it will often be
      from a graphical menu where you simply click on the
      <application>GIMP</application> entry. This usually launches
      <application>GIMP</application> in its default form without any
      images loaded. If you launch <application>GIMP</application> from
      a console, or edit the command used to launch it from the menu,
      you can pass several options to it. These are in the format:
      <command>gimp</command> <optional>options</optional> 
      <replaceable>files ...</replaceable>
 -->
    </para>

    <para>
      <variablelist>
	<title>GIMP オプション</title>
	<varlistentry>
	  <term><cmdsynopsis><arg>-h</arg> <arg>--help</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
利用可能なオプションのリストを表示し、それぞれの簡単な説明を表示します。
<!-- 
	      Displays a list of available options, and gives a terse
	      description of each one.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-v</arg> <arg>--version</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
インストールした <application>GIMP</application> のバージョンナンバーを表示します。
<!-- 
	      Prints the version number of the installed
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-b</arg> <arg>--batch <replaceable>commands</replaceable></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
バッチ (非インタラクティブ) モードで <application>GIMP</application> を実行します。<option>-b</option> に続けて引数を任意のコマンドとして使うことで <application>script-fu</application> で対話的に処理するため <application>GIMP</application> に渡します。
<!-- 
	      Runs <application>GIMP</application> in batch
	      (non-interactive) mode.
              Any parameters used as commands for <option>-b</option>
              will be passed to <application>The GIMP</application> to
              be interpreted by <application>script-FG</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-g</arg> <arg>--gimprc <option>gimprc</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
通常は <filename>~/gimp-1.2/gimprc</filename> にあるデフォルトの代わりに代替の <filename>gimprc</filename> (<application>GIMP</application> 設定ファイル) を使用します。これはプラグインパスやマシン仕様が違っているかもしれないときに役立ちます。
<!-- 
	      Use an alternative <filename>gimprc</filename>
	      (<application>GIMP</application> settings file) instead of the
	      default which is usually located at
	      <filename>~/gimp-1.2/gimprc</filename>. This is useful
	      where plug-in paths or machine specifications may be different.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-n</arg> <arg>--no-interface</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
ユーザーインターフェイス無しで実行します。
<!-- 
	      Run without a user interface.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-r</arg> <arg>--restore-session</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
保存したセッションを復元しようとします。これはそれらが保存した状態になっていたように種々のダイアログを配置して <application>GIMP</application> を起動するでしょう。
<!-- 
	      Attempt to restore a saved session. This will start
	      <application>GIMP</application> with the various dialogs
	      as they were in the saved state.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--no-data</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
パターン、グラデーション、パレットとブラシを読み込まずに <application>GIMP</application> を起動します。これは起動時間を非常に減少させ、非インタラクティブ状態で <application>GIMP</application> を使う時にときどき便利です。
<!-- 
	      Start <application>GIMP</application> without loading
	      patterns, gradients, palettes and brushes. This
	      significantly reduces the start-up time, and is often
	      useful when using <application>GIMP</application> in
	      non-interactive situations.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--verbose</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
コンソールに起動メッセージを表示し、解析された全ての設定ファイルと読み込んだモジュールを表示します。これはデバッギング状態においてときどき便利です。
<!-- 
	      Prints startup messages to the console, showing all the
	      settings files which are parsed and the modules
	      loaded. This is often useful in debugging situations.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--no-splash</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
スプラッシュ画面を表示しません。これは読み込み時間を非常に減少させますが、スプラッシュ画面から進行状況バーを見れないでしょう。これは <cmdsynopsis><arg>--verbose</arg></cmdsynopsis> を自動的に伴います。
<!-- 
	      Do not show the splash screen. This significantly
	      decreases the load time, although you will not see the
	      progress bar from the splash screen. This automatically
	      implies <cmdsynopsis><arg>- -verbose</arg></cmdsynopsis>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--no-splash-image</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
スプラッシュ画面の一部としてスプラッシュ画像を表示しません。スプラッシュ画面で文字情報を表示するだけです。進行状況表示はまだ見ることができます。これは <application>GIMP</application> の読み込み時間を減少させます。
<!-- 
	      Do not show the splash screen image as part of the splash
	      screen. Only shows text information in the splash
	      screen. The progress indicator is still visible. This
	      decreases <application>GIMP</application>'s load time.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--no-shm</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> とそのプラグインとの間で共有メモリを使いません。共有メモリを使う代わりに、<application>GIMP</application> はパイプによってメモリを渡します。これは共有メモリを使用した状態よりもパフォーマンス低下を招くでしょう。
<!-- 
	      Do not use shared memory between
	      <application>GIMP</application> and its plug-ins. Instead
	      of using shared memory, <application>GIMP</application>
	      will send data via pipe. This will result in slower
	      performance than using shared memory.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--no-xshm</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
X 共有メモリ拡張を使いません。もし <application>GIMP</application> がリモート X サーバで表示されているなら、これは多分有効にしておく必要があります。これは X 共有メモリ拡張に正式対応をしていないあらゆる X サーバにも役に立つでしょう。これは共有メモリを有効にした状態よりもパフォーマンス低下を招くでしょう。
<!-- 
	      Do not use the X Shared Memory extension. If
	      <application>GIMP</application> is being displayed on a
	      remote X server, this probably needs to be enabled. It is
	      also useful for any X server that doesn't properly support
	      the X shared memory extension. This will result in slower
	      performance than with X shared memory enabled.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--display <option>display</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
指定した X ディスプレイを使います。
<!-- 
	      Use the specified X display.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--console-messages</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
エラーや警告のダイアログボックスをポップアップせず、代わりにコンソールにそれらを表示します。
<!-- 
	      Do not pop-up dialog boxes on errors or warning, print
	      them to the console instead.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--debug-handlers</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
致命的ではないが、全てのシグナルのためにスタックトレースプロンプトを付けたデバッグハンドラを有効にします。
<!-- 
	      Enable debug handlers which turns on the stack trace prompt
	      for all signals, not just fatal ones.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--system-gimprc <option>gimprc</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
代替のシステム共用 gimrc ファイルを使います。
<!-- 
	      Use an alternate system-wide gimprc file.
 -->
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </para>
  </sect1>

  <sect1>
    <title>GIMP Tool</title>
    <indexterm><primary>GIMP Tool</primary></indexterm>
    <?dbhtml filename="gimptool.html">

    <para>
gimptool はスクリプトとプラグインの作成とインストールを可能にする <application>GIMP</application> のためのサポートスクリプトで、<application>GIMP</application> がコンパイルされたライブラリとパスについて他のプログラムへの情報を提供することができます。書式は次の通りです。<cmdsynopsis><arg>gimptool <replaceable>options ...</replaceable></arg></cmdsynopsis>
<!-- 
      gimptool is a support script for <application>GIMP</application>
      which allows you to build and install scripts and plug-ins, and can
      provide information to other programs about the libraries and
      paths that <application>GIMP</application> was compiled with. The
      format is: <cmdsynopsis><arg>gimptool <replaceable>options ...</replaceable></arg></cmdsynopsis>
 -->
    </para>
    
    <para>
      <variablelist>
	<title>GIMP Tool オプション</title>
	<varlistentry>
	  <term><cmdsynopsis><arg>--help</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
利用可能なオプションのリストを表示し、それぞれの簡単な説明を表示します。
<!-- 
	      Displays a list of available options, and gives a terse
	      description of each one.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--version</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
インストールした <application>GIMP</application> のバージョンナンバーを表示します。
<!-- 
	      Prints the version number of the installed
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--quiet</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
作成コマンドを全く表示せずに実行します。
<!-- 
	      Runs without printing any of the build commands.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-n</arg> <arg>--dry-run</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
コマンドを表示しますが、実際にその実行はしません。テストのための予行演習をさせるのに便利です。
<!-- 
	      Print commands, but don't actually execute them. Useful
	      for making dry-runs for testing.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--bindir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した bindir を表示します。
<!-- 
	      Prints the bindir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--sbindir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した sbindir を表示します。
<!-- 
	      Prints the sbindir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--libexecdir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した libexecdir を表示します。
<!-- 
	      Prints the libexecdir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--datadir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した datadir を表示します。
<!-- 
	      Prints the datadir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--sysconfdir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した sysconfdir を表示します。
<!-- 
	      Prints the sysconfdir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--sharedstatedir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した sharedstatedir を表示します。
<!-- 
	      Prints the sharedstatedir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--localstatedir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した localstatedir を表示します。
<!-- 
	      Prints the localstatedir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--libdir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した libdir を表示します。
<!-- 
	      Prints the libdir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--infodir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した infodir を表示します。
<!-- 
	      Prints the infodir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--mandir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した mandir を表示します。
<!-- 
	      Prints the mandir used to install
	      <application>GIMP</application> man (manual) pages.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--includedir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> をインストールするために使用した includedir を表示します。
<!-- 
	      Prints the includedir used to install
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--gimpdatadir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
パターンとブラシのような、<application>GIMP</application> データファイルがインストールされた場所の実際のディレクトリを表示します。
<!-- 
	      Prints the actual directory where
	      <application>GIMP</application> data files, such as
	      patterns and brushes, were installed.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--plugindir</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> プラグインがインストールされた場所の実際のディレクトリを表示します。
<!-- 
	      Prints the actual directory where
	      <application>GIMP</application> plug-ins were installed.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--build <option>plug-in.c</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in.c をコンパイルして <application>GIMP</application> プラグインにリンクします。
<!-- 
	      Compile and link plug-in.c into a
	      <application>GIMP</application> plug-in.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--build-strip <option>plug-in.c</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in.c をコンパイル、リンクして <application>GIMP</application> プラグインに strip します。
<!-- 
	      Compile, link and strip plug-in.c into a
	      <application>GIMP</application> plug-in.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install <option>plug-in.c</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in.c をコンパイル、リンクしてユーザーの個人用プラグインディレクトリ - <filename>~/.gimp-1.2/plug-ins/</filename> にインストールします。
<!-- 
	      Compile, link and install plug-in.c into the users personal
	      plug-in directory - <filename>~/.gimp-1.2/plug-ins/</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-strip <option>plug-in.c</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in.c をコンパイル、リンク、strip してユーザーの個人用プラグインディレクトリ - <filename>~/.gimp-1.2/plug-ins/</filename> にインストールします。
<!-- 
	      Compile, link, strip and install plug-in.c into the
	      users personal
	      plug-in directory - <filename>~/.gimp-1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-admin <option>plug-in.c</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in.c をコンパイル、リンクしてシステム共用のプラグインディレクトリ - <filename>$PREFIX/lib/gimp/1.2/plug-ins</filename> にインストールします。
<!-- 
	      Compile, link, and install plug-in.c into the system-wide
	      plug-in directory - 
	      <filename>$PREFIX/lib/gimp/1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-bin <option>plug-in</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
既にコンパイルとリンクが済んだプラグインをユーザー個人用のプラグインディレクトリ - <filename>~/.gimp-1.2/plug-ins</filename> にインストールします。
<!-- 
	      Install a plug-in, which has already been compiled and
	      linked, into the users personal plug-in directory - 
	      <filename>~/.gimp-1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-admin-bin <option>plug-in</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
既にコンパイルとリンクが済んだプラグインをシステム共用のプラグインディレクトリ - <filename>$PREFIX/gimp/1.2/plug-ins</filename> にインストールします。
<!-- 
	      Install a plug-in, which has already been compiled and
	      linked, into the system-wide plug-in directory - 
	      <filename>$PREFIX/gimp/1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-bin-strip <option>plug-in</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
既にコンパイルとリンクが済んで strip したプラグインをユーザー個人用のプラグインディレクトリ - <filename>~/.gimp-1.2/plug-ins</filename> にインストールします。
<!-- 
	      Install a stripped plug-in, which has already been compiled
	      and linked, into the users personal plug-in directory - 
	      <filename>~/.gimp-1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term>
	    <cmdsynopsis><arg>--install-admin-bin-strip <option>plug-in</option></arg></cmdsynopsis> 
	  </term>
	  <listitem>
	    <para>
既にコンパイルとリンクが済んで strip したプラグインをシステム共用のディレクトリ - <filename>$PREFIX/gimp/1.2/plug-ins</filename> にインストールします。
<!-- 
	      Install a stripped plug-in, which has already been compiled
	      and linked, into the system-wide plug-in directory - 
	      <filename>$PREFIX/lib/gimp/1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--install-script <option>script.scm</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
script.scm をユーザー個人用のスクリプトディレクトリ - <filename>~/.gimp-1.2/scripts</filename> にインストールします。
<!-- 
	      Install script.scm, into the users personal scripts
	      directory - <filename>~/.gimp-1.2/scripts</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term>
	    <cmdsynopsis><arg>--install-admin-script <option>script.scm</option></arg></cmdsynopsis>
	  </term>
	  <listitem>
	    <para>
script.scm をシステム共用のスクリプトディレクトリ - <filename>$PREFIX/share/gimp/scripts</filename> にインストールします。
<!-- 
	      Install script.scm, into the system-wide scripts
	      directory - <filename>$PREFIX/share/gimp/scripts</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--uninstall-bin <option>plug-in</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in をユーザー個人用のプラグインディレクトリ - <filename>~/.gimp-1.2/plug-ins</filename> からアンインストールします。
<!-- 
	      Uninstall a plug-in from a users personal plug-in directory
	      - <filename>~/.gimp-1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--uninstall-admin-bin <option>plug-in</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
plug-in をシステム共用のプラグインディレクトリ - <filename>$PREFIX/lib/gimp/1.2/plug-ins</filename> からアンインストールします。
<!-- 
	      Uninstall a plug-in from the system-wide plug-in directory
	      - <filename>$PREFIX/lib/gimp/1.2/plug-ins</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--uninstall-script <option>script.scm</option></arg></cmdsynopsis></term>
	  <listitem>
	    <para>
スクリプトをユーザー個人用のスクリプトディレクトリ - <filename>~/.gimp-1.2/scripts</filename> からアンインストールします。
<!-- 
	      Uninstall a script from a users personal scripts directory
	      - <filename>~/.gimp-1.2/scripts</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--uninstall-admin-script <option>script.scm</option></arg></cmdsynopsis>
	  </term>
	  <listitem>
	    <para>
スクリプトをシステム共用のスクリプトディレクトリ - <filename>$PREFIX/share/gimp/scripts</filename> からアンインストールします。
<!-- 
	      Uninstall a script from the system-wide scripts directory
	      - <filename>$PREFIX/share/gimp/scripts</filename>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--libs</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> プラグインをリンクするために必要なリンカフラグを表示します。
<!-- 
	      Print the linker flags that are necessary to link a
	      <application>GIMP</application> plug-in.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--libs-noui</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
GTK+ ライブラリを要求しない <application>GIMP</application> プラグインをリンクするために必要なリンカフラグを表示します。
<!-- 
	      Print the linker flags that are necessary to link a
	      <application>GIMP</application> plug-in which doesn't
	      require the GTK+ libraries.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--cflags</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
<application>GIMP</application> プラグインをリンクするために必要なコンパイラフラグを表示します。
<!-- 
	      Print the compiler flags that are necessary to link a
	      <application>GIMP</application> plug-in.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--cflags-noui</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
GTK+ ライブラリを必要としない <application>GIMP</application> プラグインをリンクするために必要なコンパイラフラグを表示します。
<!-- 
	      Print the compiler flags that are necessary to link a
	      <application>GIMP</application> plug-in which doesn't
	      require the GTK+ libraries.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--prefix=PREFIX</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
指定していれば、--cflags と --libs オプションのために出力を計算している時に GIMP がビルドしたインストール prefix の代わりに PREFIX を使用します。このオプションは --exec-prefix が指定されていないなら exec prefix のためにも使用されます。このオプションは --libs や --cflags オプションの前に指定されていなければなりません。
<!-- 
	      If specified, use PREFIX instead of the installation
	      prefix that GIMP was built with when computing the output
	      for the - -cflags and - -libs options. This option is also
	      used for the exec prefix if - -exec-prefix was not
	      specified.  This option must be specified before any
	      - -libs or - -cflags options.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>--exec-prefix=PREFIX</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
指定していれば、--cflags と --libs オプションのために出力を計算している時に GIMP がビルドしたインストール exec prefix の代わりに PREFIX を使用します。このオプションは --libs や --cflags オプションの前に指定されていなければなりません。
<!-- 
	      If specified, use PREFIX instead of the installation exec
	      prefix that GIMP was built with when computing the output
	      for the - -cflags and - -libs options. This option must be
	      specified before any - -libs or - -cflags options.
 -->
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </para>
  </sect1>

  <sect1>
    <title>GIMP Remote</title>
    <indexterm><primary>GIMP Remote</primary></indexterm>
    <?dbhtml filename="gimp_remote.html">

    <para>
<application>GIMP Remote</application> は GIMP のための別のサポートプログラムです。既に実行している <application>GIMP</application> へコマンドラインから画像を開くことができます。書式は次の通りです。<cmdsynopsis><arg>gimp-remote <replaceable>options ...</replaceable> <replaceable>files ...</replaceable></arg></cmdsynopsis>
<!-- 
      <application>GIMP Remote</application> is another support program
      for GIMP. It allows you to open an image from the command line in
      an already running <application>GIMP</application>. The format is:
      <cmdsynopsis><arg>gimp-remote <replaceable>options ...</replaceable>
      <replaceable>files ...</replaceable></arg></cmdsynopsis> 
 -->
    </para>
    
    <para>
      <variablelist>
	<title>GIMP Remote オプション</title>
	<varlistentry>
	  <term><cmdsynopsis><arg>-h</arg> <arg>--help</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
利用可能なオプションのリストを表示し、それぞれの簡単な説明を表示します。
<!-- 
	      Displays a list of available options, and gives a terse
	      description of each one.
 -->
	    </para>
	  </listitem>
	</varlistentry>
	 
	<varlistentry>
	  <term><cmdsynopsis><arg>-v</arg> <arg>--version</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
インストールした <application>GIMP</application> のバージョンナンバーを表示します。
<!-- 
	      Prints the version number of the installed
	      <application>GIMP</application>.
 -->
	    </para>
	  </listitem>
	</varlistentry>

	<varlistentry>
	  <term><cmdsynopsis><arg>-n</arg> <arg>--new</arg></cmdsynopsis></term>
	  <listitem>
	    <para>
もしこれが利用可能になっていなければ <application>GIMP</application> の新規インスタンスを開きます。
<!-- 
	      Opens a new instance of <application>GIMP</application> if
	      one is not already available.
 -->
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </para>
  </sect1>
</appendix>


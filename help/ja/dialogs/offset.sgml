<!--
<!doctype sect2 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect2 id="dialogs-offset">
  <title>オフセット</title>
  <indexterm><primary>オフセット</primary></indexterm>
  <?dbhtml filename="offset.html" dir="dialogs"?>

  <para>
オフセットは選択したピクセル数によって現在のレイヤーまたはチャンネルを上、下、左、右に移動します。縁がレイヤーの大きさを超える時は、回り込みをするかしないか決めることができます。オフセットはレイヤーやフローティング選択を画像の指定した位置に置くために使われます。
<!-- 
    Offset moves the current layer or channel
    up, down, left, or right by the chosen number of pixels. When the
    edges overrun the dimensions of the layer, you can decide if they
    are wrapped or not. Offset can be used to
    place layers or floating selections at a specific position in the
    image.
 -->
  </para>

  <para>
きっちり正確な量だけレイヤーを移動したいときやレイヤー境界を拡張しないで移動したいときにオフセットは役に立ちます。このコマンドの他の重要な応用はパターンのために継ぎ目の無いタイルを作成するときです。
<!-- 
    Offset is useful if you want to move layers
    a very exact amount or if you'd like to move them without
    extending the layer border. The other important application of
    this command is to create seamless tiles for patterns.
 -->
  </para>

  <para>
<quote><guibutton>回り込み</guibutton></quote> ボタンがチェックされていると、レイヤー境界の外側に移動した画像の部分は画像のもう一方に現れるでしょう。これをしたくなければ、背景色か透明で何も無い空間を塗りつぶすことを選べます。
<!-- 
    If the <quote><guibutton>Wrap Around</guibutton></quote> button is
    checked, the parts of the image that move outside the layer border
    will turn up on the other side of the image. If you don't want
    this, you can choose to fill the empty area with the background
    color or with transparency.
 -->
  </para>

  <bridgehead>追加情報</bridgehead>
  
  <para>
    デフォルトのキーボードショートカット: <keycombo><keycap>Shift</keycap>
    <keycap>Ctrl</keycap> <keycap>O</keycap></keycombo>
  </para>
</sect2>  

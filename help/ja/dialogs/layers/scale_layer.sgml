<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="dialogs-layers-scale-layer">
  <title>レイヤー拡大縮小</title>
  <indexterm><primary>レイヤー拡大縮小</primary></indexterm>
  <?dbhtml filename="scale_layer.html" dir="dialogs/layers"?>

  <para>
レイヤーの中身を拡大縮小します。そしてまた、レイヤーの境界も大きくなったり小さくなったりします。"レイヤー拡大縮小" と<link linkend="dialogs-layers-layer-boundary-size">レイヤー境界の大きさ</link>との違いは、レイヤーの中身を拡大縮小せずに、レイヤーを大きくしたり小さくしたりするかどうかにあります (つまり、レイヤー境界の大きさでは、レイヤーのまわりに一定の余白を加えるだけか、レイヤーを切り抜くだけ)。
<!-- 
    Will scale the layer content, and also make the layer boundary
    either size smaller or larger. The difference between "Scale
    Layer" and <link linkend="dialogs-layers-layer-boundary-size">Set
    Layer Boundary Size</link> is that "Set Layer Boundary Size" will
    enable you to have a smaller or bigger layer without scaling the
    layer content (i.e. it will only add some space to on around the
    layer or clip the layer).
 -->
  </para>

  <note>
    <para>
"レイヤー拡大縮小" は、現在アクティブなレイヤーだけを拡大縮小します。<link linkend="dialogs-scale-image">画像の拡大縮小</link>は、画像にある全レイヤーを拡大縮小するでしょう。
<!-- 
      "Scale Layer" will only scale the currently active layer. <link
      linkend="dialogs-scale-image">Scale Image</link> will scale all
      layers in the image.
 -->
    </para>
  </note>

  <para>
新しいレイヤーの大きさは、サイズまたは比率を変えることによって設定できます。<link linkend="dialogs-layers-new-layer">新規レイヤー</link>ダイアログにあるのとまったく同じオプションがあります。
<!-- 
    You can set the new layer size either by altering the size or
    ratio. You have have exactly the same options as in the <link
    linkend="dialogs-layers-new-layer">New Layer</link> dialog.
 -->
  </para>
</sect1>

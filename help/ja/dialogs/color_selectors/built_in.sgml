<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="dialogs-color-selectors-built-in">
  <title>標準 GIMP 色選択</title>
  <indexterm><primary>標準 GIMP 色選択</primary></indexterm>
  <indexterm><primary>色選択</primary></indexterm>
  <?dbhtml filename="built_in.html" dir="dialogs/color_selectors"?>

  <para>
<guibutton>色のスペクトル領域</guibutton>の隣にある H (色相) をクリックして、右側の大きなカラーボックスの中の<guibutton>十字</guibutton>をあなたの欲しい正確な色までドラッグすることにより、手動で色を変えることができます。もしも、特定の <acronym>RGB</acronym> (赤 Red, 緑 Green, 青 Blue) カラー、または、<acronym>HSV</acronym> (色相 Hue, 彩度 Saturation, 明度 Value) カラーが欲しいときは、<acronym>HSV</acronym> または <acronym>RGB</acronym> のパラメータ領域に正確な値を入力するか、スライダーをドラッグして色を指定してください。
<!-- 
    You can change the color manually by clicking on a hue in the
    <guibutton>spectrum color field</guibutton> to the right, and then
    dragging the <guibutton>cross</guibutton> in the large color box
    on the left to the exact color you want. If you want a specific
    <acronym>RGB</acronym> (Red, Green, Blue) or
    <acronym>HSV</acronym> (Hue, Saturation, Value) color, type the
    exact value in the <acronym>HSV</acronym> or
    <acronym>RGB</acronym> parameter fields or drag the sliders to
    specify a color.
 -->
  </para>
      
  <para>
色選択ダイアログを開いたときに、デフォルトでは、<abbrev>H</abbrev> (色相) チャンネルが色領域に表示されています。しかし、あなたの欲しい色をほかのチャンネルで探すという選択があります。特別の色を探しているときには、チャンネルを <abbrev>S</abbrev> (彩度) か <abbrev>V</abbrev> (明度) に変えることが、非常に役立つかもしれません。こうすると、色のスペクトルは、まったく違ったやり方で表示されるため、もっと有効に色が見つけられるかもしれません。
<!-- 
    The <abbrev>H</abbrev> (hue) channel is displayed in the color
    fields by default when you open the Color
    Selection dialog.  However, you can choose to search
    in another channel for the color you want. It can be very useful
    to change the channel to <abbrev>S</abbrev> (saturation) or
    <abbrev>V</abbrev> (value) when you're searching for a specific
    color. The color spectrum will be displayed in a quite different
    way by doing this and you might find it more useful.
 -->
  </para>
      
  <para>
ダイアログの下のほうに、三組の十六進数として表示された現在の色があります。ウェブデザイナー向けの機能です。それを選択してマウスの<mousebutton>中</mousebutton>ボタンでペーストするだけで、この色のコードを <acronym>HTML</acronym> ドキュメントに入れることができます。<application>GIMP for Windows</application> を使っているなら、同じ結果を得るためには、現在の色の欄をマウスでドラッグして選択してから、<keycombo action="simul"><keycap>Ctrl</keycap><keycap>C</keycap></keycombo> (コピー) と <keycombo action="simul"><keycap>Ctrl</keycap><keycap>V</keycap></keycombo> (貼り付け) でやらねばなりません。
<!-- 
    In the bottom of the dialog you will find the current color
    specified as a hexedecimal triplet. The function is targeted at
    web developers &mdash; to get the color code into your
    <acronym>HTML</acronym> document simply select it and paste it in
    with your <mousebutton>middle</mousebutton> mouse button. If you are
    using <application>GIMP for Windows</application> you must use <keycombo
    action="simul"><keycap>Ctrl</keycap><keycap>C</keycap></keycombo>
    and <keycombo
    action="simul"><keycap>Ctrl</keycap><keycap>V</keycap></keycombo>
    to achieve the same result.
 -->
  </para>
</sect1>

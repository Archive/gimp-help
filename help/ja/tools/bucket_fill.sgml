<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="tools-bucket-fill">
  <title>塗りつぶし</title>
  <indexterm><primary>塗りつぶし</primary></indexterm>
  <indexterm><primary>描画ツール</primary></indexterm>
  <?dbhtml filename="bucket_fill.html" dir="tools"?>

  <sect2 id="tools-bucket-fill-overview">
    <title>
      概要
    </title>
    
    <para>
<guibutton>塗りつぶし</guibutton> <inlinemediaobject><imageobject><imagedata fileref="../images/tools/tool_fill.png" format="png"></imageobject></inlinemediaobject> ツールはレイヤー領域や選択領域を色かパターンで塗りつぶすために使用します。
<!-- 
      The <guibutton>Bucket Fill</guibutton>
      <inlinemediaobject>
        <imageobject>
    <imagedata fileref="../images/tools/tool_fill.png" format="png">
        </imageobject>
      </inlinemediaobject>
      tool is used to fill areas of a layer or selection with either
      color or a pattern.
 -->
    </para>
  </sect2>

  <sect2 id="tools-bucket-fill-options">
    <title>
      塗りつぶしツールのオプション
    </title>
    
    <variablelist><title>塗りつぶしの設定</title>
      <varlistentry><term>レイヤー結合色</term>
        <listitem>
          <para>
このオプションは全レイヤーからのサンプリングを切り替えます。<guibutton>レイヤー結合色</guibutton>がアクティブになっていれば、閾値を使った色情報がより上にあるものを見つけてチェックしても、より下のレイヤーに基づいて塗りつぶせます。単に低いレベルを選んで、上のレイヤーがカラーウェイティングのために可視的であるようにしてください。
<!-- 
            This option toggles the sampling from all layers. If
            <guibutton>Sample Merged</guibutton> is active, fills can be
            made on a lower layer, while the color information used for
            threshold checking is located further up. Simply select the
            lower level and ensure that a layer above is visible for
            color weighting.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>閾値</term>
        <listitem>
          <para>
<guibutton>閾値</guibutton>スライダーは、塗りつぶしの境界を測って色を重ねるためのレベルを設定します。設定を高くするほど多色の画像をたくさん塗りつぶし、逆に設定を低くするほど少しの領域を塗りつぶします。
<!-- 
            The <guibutton>Threshold</guibutton> slider sets the level
            at which color weights are measured for fill boundaries. A
            higher setting will fill more of a multi colored image and
            conversely, a lower setting will fill less area.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>塗りつぶしの種類</term>
        <listitem>
          <itemizedlist>
            <listitem>
              <para>
<guibutton>前景色塗り</guibutton>: このオプションは塗りつぶし色に現在選択している前景色をセットします。
<!-- 
                <guibutton>FG Color Fill</guibutton>: This sets the fill
                color to the currently selected foreground color.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>背景色塗り</guibutton>: このオプションは塗りつぶし色に現在選択している背景色をセットします。
<!-- 
                <guibutton>BG Color Fill</guibutton>: This sets the fill
                color to the currently selected background color.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>パターン塗り</guibutton>: このオプションは塗りつぶし色に現在選択しているパターンをセットします。
<!-- 
                <guibutton>Pattern Fill</guibutton>: This option sets the
                fill color to the currently selected pattern.
 -->
              </para>
              <para>
参照: <link linkend="dialogs-pattern-selection">パターン選択</link>
<!-- 
                See also:
                <link linkend="dialogs-pattern-selection">
                  Pattern Selection
                </link>.
 -->
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>

    
    <bridgehead>追加情報</bridgehead>
    <para>
      デフォルトのショートカット: <keycombo><keycap>Shift</keycap>
      <keycap>B</keycap></keycombo>
    </para>
    <para>
キー修飾: <keycombo><keycap>Ctrl</keycap></keycombo> は<guibutton>背景色塗り</guibutton>と<guibutton>前景色塗り</guibutton>の使用をその場で切り替えます。
<!-- 
      The key modifier:
      <keycombo>
        <keycap>Ctrl</keycap>
      </keycombo>
      will toggle the use of <guibutton>BG Color Fill</guibutton> or
      <guibutton>FG Color Fill</guibutton> on the fly.
 -->
    </para>
  </sect2>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="tools-by-color-select">
  <title>色による選択</title>
  <indexterm><primary>色による選択</primary></indexterm>
  <indexterm><primary>選択ツール</primary></indexterm>
  <?dbhtml filename="by_color_select.html" dir="tools"?>

  <sect2 id="tools-by-color-select-overview">
    <title>
      概要
    </title>

    <para>
<guilabel>色による選択</guilabel>ツールは色を使用して現在のレイヤーに選択領域を作ることができます。<link linkend="tools-fuzzy-select">ファジー選択ツール</link>とは違って、<guibutton>色による選択</guibutton>ツールは隣接領域だけでなく、レイヤーの全域で選択領域を作ります。
<!-- 
      The <guilabel>Select by Color</guilabel> tool is capable of
      selecting areas of the current layer using color. Unlike the
      <link linkend="tools-fuzzy-select">
        Fuzzy Selection Tool
      </link>
      , the <guibutton>Select by Color</guibutton> tool will make
      selections across the entire layer, not just adjoining areas.
 -->
    </para>
  </sect2>

  <sect2 id="tools-by-color-select-options">
    <title>
      色による選択のオプション
    </title>

    <para>
<guibutton>色による選択</guibutton>を非常に強力にする多くのオプションがあります。
<!-- 
      There are many options that make <guibutton>Select by
      Color</guibutton> very powerful.
 -->
    </para>

    <variablelist><title>色による選択の設定</title>
      <varlistentry><term>選択プレビュー</term>
        <listitem>
          <para>
ダイアログの左側は白黒を使用して現在の選択をプレビューする領域です。黒の領域は非選択で、白の領域は選択されている範囲です。
<!-- 
            To the left of the dialog is an area which previews the
            current selection using black or white. Black areas are
            unselected and white areas are selected regions.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>選択モード</term>
        <listitem>
          <para>
四つの選択モードがあります。
<!-- 
            There are four selection modes:
 -->
          </para>
          <itemizedlist>
            <listitem>
              <para>
<guibutton>置換</guibutton>: このモードは画像ウィンドウでクリックするたびに新しい選択領域を作成します。
<!-- 
                <guibutton>Replace</guibutton>: This mode will create a
                new selection region with each click in the image
                window.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>追加</guibutton>: このモードは画像ウィンドウで追加クリックするたびに現在の選択領域に追加<!-- ***累加、累積*** -->します。
<!-- 
                <guibutton>Add</guibutton>: This mode will add to the
                current selection with each additional click in the
                image window.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>減算</guibutton>: このモードは、既存の選択領域があればそこから新しく選択した領域を取り除きます。
<!-- 
                <guibutton>Subtract</guibutton>: This mode removes the
                newly selected area from the existing selection area if
                it exists.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>共通部分</guibutton>: このモードは選択領域の結果を決めるために論理的な交差を使用します。新しい領域を示すために画像ウィンドウでクリックした後で、オーバーラップしているそれらの範囲だけを含むように選択領域が減少します。それはつまり、古い選択領域と新しい選択領域が交差するところです。
<!-- 
                <guibutton>Intersect</guibutton>: This mode uses logical
                intersection to determine the resulting selection area.
                After clicking in the image window to indicate the new
                region, the selection area will be reduced to include
                only those regions with were overlapping. That is, where
                the old selection area and the new selection area
                intersected.
 -->
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>ファジー度の閾値</term>
        <listitem>
          <para>
<guibutton>ファジー度の閾値</guibutton>スライダーは閾値の選択を調整します。閾値を高くするほどクリックする毎に多くの範囲が選択されます。
<!--クリックする毎に、というのも意味不明だが。-->
<!-- 
            The <guibutton>Fuzziness Threshold</guibutton> slider
            adjusts the selection threshold. Higher thresholds result in
            greater areas being selected with each click.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>選択</term>
        <listitem>
          <para>
この選択の三つのボタンは選択領域をすぐに修正します。
<!-- 
            The three buttons in this section modify the selection area
            quickly.
 -->
          </para>
          <itemizedlist>
            <listitem>
              <para>
<guibutton>反転</guibutton>: このボタンは現在の選択領域を反転します。
<!-- 
                <guibutton>Invert</guibutton>: This button inverts the
                current selection.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>全て</guibutton>: このボタンはレイヤー全体を選択します。
<!-- 
                <guibutton>All</guibutton>: This button selects the
                entire layer.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>なし</guibutton>: このボタンは現在の選択を取り除きます。
<!-- 
                <guibutton>None</guibutton>: This button removes the
                current selection.
 -->
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>リセット / 閉じる</term>
        <listitem>
          <para>
<guibutton>リセット</guibutton>ボタンは選択オプションをデフォルトにリセットします。
<!-- 
            The <guibutton>Reset</guibutton> button resets the selection
            options to the defaults.
 -->
          </para>
          <para>
<guibutton>閉じる</guibutton>ボタンは選択領域を残したまま<guibutton>色による選択</guibutton>ダイアログを閉じます。
<!-- 
            The <guibutton>Close</guibutton> button closes the
            <guibutton>Select by Color</guibutton> dialog maintaining
            the selection area.
 -->
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="tools-flip">
  <title>鏡像反転</title>
  <indexterm><primary>鏡像反転</primary></indexterm>
  <indexterm><primary>変換ツール</primary></indexterm>
  <?dbhtml filename="flip.html" dir="tools"?>

  <sect2 id="tools-flip-overview">
    <title>概要</title>
    <para>
<guibutton>鏡像反転</guibutton> <inlinemediaobject><imageobject><imagedata fileref="../images/tools/tool_flip.png" format="png"></imageobject></inlinemediaobject> ツールは水平か垂直のどちらかで、レイヤーや選択領域を鏡像反転することができます。
<!-- 
      The <guibutton>Flip</guibutton>
      <inlinemediaobject>
        <imageobject>
    <imagedata fileref="../images/tools/tool_flip.png" format="png">
        </imageobject>
      </inlinemediaobject>
      tool provides the ability to flip layers or selections either
      horizontally or vertically.
 -->
    </para>

    <tip>
      <para>
選択領域を鏡像反転した時、新規レイヤーが作成されます。新規レイヤーは選択領域を作成した古いレイヤーから <quote>切り抜いた</quote> 領域を持ちます。
<!-- 
        When flipping selections, a new layer will be created and the
        old layer that the selection was created on will have that area
        <quote>cut</quote> from it.
 -->
      </para>
    </tip>
  </sect2>

  <sect2 id="tools-flip-options">
    <title>鏡像反転ツールのオプション</title>

    <variablelist><title>鏡像反転ツールの設定</title>
      <varlistentry><term>機能切り替え</term>
        <listitem>
          <para>
<guilabel>機能切り替え</guilabel>の設定は<guibutton>水平</guibutton>方向か<guibutton>垂直</guibutton>方向のどちらかでの鏡像反転を制御します。この切り替えはキー修飾を使用して切り替えることもできます。
<!-- 
            The <guilabel>Tool Toggle</guilabel> settings control
            flipping in either a <guibutton>Horizontal</guibutton>
            direction or a <guibutton>Vertical</guibutton> one. This
            toggle can also be switched using a key modification.
 -->
          </para>
        </listitem>
      </varlistentry>
    </variablelist>

    <bridgehead>追加情報</bridgehead>
    <para>
      デフォルトのショートカット: <keycombo><keycap>Shift</keycap>
      <keycap>F</keycap></keycombo>
    </para>
    <para>
このツールには一つの修飾があります。
<!-- 
      There is one modifier for this tool.
 -->
    </para>
    <itemizedlist>
      <listitem>
        <para>
キーの組み合わせ: <keycombo><keycap>Ctrl</keycap> <mousebutton>ボタン1</mousebutton></keycombo> は現在選択している方向とはもう一方の方向で鏡像反転します。
<!-- 
          The key combination:
          <keycombo>
            <keycap>Ctrl</keycap>
            <mousebutton>Button1</mousebutton>
          </keycombo>
          will flip in the other direction to the currently selected
          one.
 -->
        </para>
      </listitem>
    </itemizedlist>
  </sect2>
</sect1>

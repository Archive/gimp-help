<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="tools-eraser">
  <title>消しゴム</title>
  <indexterm><primary>消しゴム</primary></indexterm>
  <indexterm><primary>描画ツール</primary></indexterm>
  <?dbhtml filename="eraser.html" dir="tools"?>
  
  <sect2 id="tools-eraser-overview">
    <title>概要</title>
    <para>
<guibutton>消しゴム</guibutton> <inlinemediaobject><imageobject><imagedata fileref="../images/tools/tool_eraser.png" format="png"></imageobject></inlinemediaobject> は現在のレイヤー、選択領域や、画像から色の塊を取り除くために使用します。<guibutton>消しゴム</guibutton>を<guibutton>背景</guibutton>レイヤーで使用すると、消しゴムは色領域を取り除き、そこを現在の背景色で置き換えます。通常のフローティングレイヤーで使用すると、色は透明で置き換えられます。同様のルールがアルファ無しの画像に適用されます。
<!-- 
      The <guibutton>Eraser</guibutton>
      <inlinemediaobject>
        <imageobject>
    <imagedata fileref="../images/tools/tool_eraser.png" format="png">
        </imageobject>
      </inlinemediaobject>
      is used to remove blocks of color from the current layer,
      selection, or image. If the <guibutton>Eraser</guibutton> is used
      on on the <guibutton>Background</guibutton> layer, the eraser will
      remove color areas and replace them with the current background
      color. If used on a normal floating layer, the color will be
      replaced with transparency. The same rules apply to non-alpha
      images.
 -->
    </para>
  </sect2>

  <sect2 id="tools-eraser-options">
    <title>消しゴムツールのオプション</title>

    <variablelist><title>消しゴムの設定</title>
      <varlistentry><term>重ね塗り</term>
        <listitem>
          <para>
<guibutton>重ね塗り</guibutton>のモードはそれぞれのブラシストロークを直接アクティブレイヤーに描画します。このオプションを設定していなければ、そこにはアクティブレイヤーで構成されるキャンバスバッファがあります。
<!-- 
            <guibutton>Incremental</guibutton> mode paints each brush
            stroke directly onto the active layer. Without this option
            set, there is a canvas buffer that is composited with the
            active layer.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>圧力感度</term>
        <listitem>
          <para>
<guilabel>圧力感度</guilabel>の欄はこのオプションが対応する入力デバイスのための感度レベルを設定します。
<!-- 
            The <guilabel>Pressure Sensitivity</guilabel> section sets
            the sensitivity levels for input devices that support this
            option.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>ハードエッジ</term>
        <listitem>
          <para>
デフォルトの<guibutton>消しゴム</guibutton>ツールは消去した領域の端を柔らかくします。<guibutton>ハードエッジ</guibutton>トグルはこの振る舞いを変更します。このオプションをアクティブにしている間に消去した領域は使用したブラシの端を柔らかくせずに消去します。
<!-- 
            By default the <guibutton>Eraser</guibutton> tool softens
            the edges of and area erased. The <guibutton>Hard
            Edge</guibutton> toggle changes this behavior. Any area
            erased while this option is active will be erased with no
            softening of the edge of the brush used.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>あぶり出し</term>
        <listitem>
          <para>
<guibutton>消しゴム</guibutton>ツールの<guibutton>あぶり出し</guibutton>機能は画像の領域をあぶり出すことができます。この機能はアルファチャンネルがある画像を使用している時だけ作用します。
<!-- 
            The <guibutton>Anti Erase</guibutton> function of the
            <guibutton>Erase</guibutton> tool can un-erase areas of an
            image. This feature only works when used on images with an
            alpha channel.
 -->
          </para>
        </listitem>
      </varlistentry>
    </variablelist>

    <bridgehead>追加情報</bridgehead>
    <para>
      デフォルトのショートカット: <keycombo><keycap>Shift</keycap>
      <keycap>E</keycap></keycombo>
    </para>
    <itemizedlist>
      <listitem>
        <para>
キー修飾: <keycombo><keycap>Ctrl</keycap></keycombo> は<guibutton>あぶり出し</guibutton>と通常の消しゴムモードとを切り替えます。
<!-- 
          The key modifier:
          <keycombo>
            <keycap>Ctrl</keycap>
          </keycombo>
          toggles between <guibutton>Anti Erase</guibutton> and normal
          erase modes.
 -->
        </para>
      </listitem>
    </itemizedlist>
  </sect2>
</sect1>

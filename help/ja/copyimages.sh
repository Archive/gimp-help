#!/bin/bash

cp --parents stylesheet-images/*.* gimp 
cp --parents images/*.* gimp
cp --parents images/tools/*.* gimp

# Here is the old stuff.
#cp --parents image/images/*.* gimp 
#cp --parents paths/images/*.* gimp
#cp --parents layers/images/*.* gimp
#cp --parents layers/stack/images/*.* gimp
#cp --parents dialogs/images/*.* gimp
#cp --parents dialogs/paths/images/*.* gimp
#cp --parents dialogs/preferences/images/*.* gimp
#cp --parents dialogs/channels/images/*.* gimp
#cp --parents channels/images/*.* gimp
#cp --parents filters/images-C/*.* gimp
#cp --parents image/images-C/*.* gimp
#cp --parents layers/images-C/*.* gimp
#cp --parents toolbox/images-C/*.* gimp

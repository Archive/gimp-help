<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]> 
-->

<sect1 id="filters-ripple">
  <title>波紋</title>
  <indexterm><primary>波紋</primary></indexterm>
  <?dbhtml filename="ripple.html" dir="filters"?>

  <simplesect>
    <title>概要</title>
    
    <para>
<quote>波紋</quote> フィルタは画像を水面が波だっているように歪ませることができます。
<!-- 
      The <quote>Ripple</quote> Filter allows the distortion of an image
      to look like a disturbed water surface.
 -->
    </para>
  </simplesect>

  <simplesect>
    <title>波紋のオプション</title>
    
    <variablelist><title>波紋の設定</title>
      <varlistentry><term><guilabel>オプション</guilabel></term>
        <listitem>
          <para>
<link linkend="glossary-anti-aliasing">アンチエイリアシング</link>ボックスにチェックを入れると、波紋の縁のぎざぎざの線が画像をぼかさずに滑らかなるようにします。
<!-- 
      	    Checking the
            <link linkend="glossary-anti-aliasing">
              Antialiasing
            </link>
            box renders jagged lines at the edge of the ripples smooth
            without blurring the image.
 -->
          </para>
      	  <para>
<guibutton>タイルメモリを保持する</guibutton>は、画像がパターンのようにタイル化可能であるところでは、最終的な歪んだ画像もタイル化可能になります。全く切れ目が無い連続した画像を作成するため、画像のコピーを並べて置くことができます。<!--何でタイル*メモリ*なのかな。あっちを連想するような。-->
<!-- 
            The <guibutton>Retain Tileability</guibutton> ensures that
            where an image is tileable, such as a pattern, the final
            distorted image will also be tileable. copies of the image
            can be placed side by side to create a continuous image
            without any break.
 -->
          </para>
      	</listitem>
      </varlistentry>			
      <varlistentry><term><guibutton>向き</guibutton></term>
        <listitem>
          <para>
どの方向に波紋が発生するかを定義します。水平か垂直方向のどちらかです。
<!-- 
      	    Defines which way the ripples occur. Either
      	    horizontal or vertical.
 -->
          </para>
        </listitem>
      </varlistentry>			
      <varlistentry><term><guibutton>周辺部</guibutton></term>
        <listitem>
          <para>
波紋効果を適用する時、フィルタは選択領域の境界の中に歪みを発生させるので、欠けている選択領域の一方の縁で小さな領域となります。
<!-- 
      	    When applying a ripple effect, the tool performs the distortion
      	    within the confines of the selection so there will be
      	    small areas at one edge of the selection which are
      	    missing.
 -->
         </para>
      	 <para>
<guibutton>黒</guibutton>を選ぶと均一な黒でその領域を塗りつぶします。<guibutton>ぼかす</guibutton>を選ぶと広げたピクセルでこの領域を塗りつぶします。<guibutton>回り込み</guibutton>は画像のもう一つの側から失われたピクセルでブランク領域を塗りつぶします。
<!-- 
           Selecting <guibutton>Black</guibutton> fills in these area
           with solid black, <guibutton>Smear</guibutton> will fill this
           area with pixels stretched to fill, and
           <guibutton>Wrap</guibutton> fills the blank area with the
           pixels that were lost from the other side of the image.
 -->
      	 </para>
      	</listitem>
      </varlistentry>			
      <varlistentry><term><guibutton>波の種類</guibutton></term>
        <listitem>
          <para>
波の種類はどのように波のカーブが計算されるのかを定義します。<guibutton>鋸歯</guibutton>は尖った波を描画します。<guibutton>Sin</guibutton> はソフトな波を描画します 。
<!-- 
            The wave type defines how the curve of the wave is
            calculated. <guibutton>Sawtooth</guibutton> renders a
            sharper wave. <guibutton>Sine</guibutton> renders a softer
            wave.
 -->
          </para>
      	</listitem>
      </varlistentry>			
      <varlistentry><term><guibutton>パラメータ設定</guibutton></term>
        <listitem>
          <para>
波長の設定は波がどのくらいの長さであるか、つまり 2 つの頂上の間の距離を設定します。振幅は波がどのくらい高いか、つまり中間と頂上の間の垂直距離を定義します。
<!-- 
            Setting the Period controls how long the waves are or the
            distance between two crests. Amplitude defines how high the
            wave is or the vertical distance between the mean and a
            crest.
 -->
          </para>
      	</listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-grid">
  <title>格子</title>
  <indexterm><primary>格子</primary></indexterm>
  <?dbhtml filename="grid.html" dir="filters"?>

  <simplesect>
    <title>概要</title>
    
    <para>
<guilabel>格子</guilabel>フィルタは画像上に注文通りの格子を配置します。格子の外観はすべてカスタマイズできます。
<!-- 
      The <guilabel>Grid</guilabel> filter places a custom grid over the
      image. All aspects of the grid can be customized.
 -->
    </para>
  </simplesect>

  <simplesect>
    <title>格子のオプション</title>
    
    <variablelist><title>格子の設定</title>
      <varlistentry><term>水平 / 鉛直</term>
        <listitem>
          <para>
<guilabel>水平</guilabel>と、<guilabel>鉛直</guilabel>の設定は<guilabel>幅</guilabel>、<guilabel>間隔</guilabel>と、<guilabel>オフセット</guilabel>の調整に使われます。<guilabel>幅</guilabel>は格子を描画する時の線幅を設定します。<guilabel>間隔</guilabel>は格子線の間の距離に影響します。<guilabel>オフセット</guilabel>は描画が始まる、画像の左上の角からのオフセットを決めます。<guilabel>オフセット</guilabel>設定の下には、格子の線の色を設定する<guilabel>色ボタン</guilabel>があります。それらの設定の全ては<guilabel>水平</guilabel>と、<guilabel>鉛直</guilabel>の格子の線を均衡させるためにお互いを <quote>ロック</quote> したり、個別に制御するため <quote>アンロック</quote> したりすることができます。
<!-- 
            The <guilabel>Horizontal</guilabel>, and
            <guilabel>Vertical</guilabel> settings are used to adjust,
            <guilabel>Width</guilabel>, <guilabel>Spacing</guilabel>,
            and <guilabel>Offset</guilabel>. <guilabel>Width</guilabel>
            sets the line width when rendering the grid.
            <guilabel>Spacing</guilabel> affects the distance between
            the grid lines. <guilabel>Offset</guilabel> determines the
            offset from the top-left corner of the image, from which
            rendering will begin. Located below the
            <guilabel>Offset</guilabel> settings, are located the
            <guilabel>Color Wells</guilabel>, which set the colors for
            the grid lines. All of these settings can be
            <quote>Locked</quote> together to balance the
            <guilabel>Horizontal</guilabel>, and the
            <guilabel>Vertical</guilabel> grid lines, or can be
            <quote>Unlocked</quote>, to provide more control.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>交点</term>
        <listitem>
          <para>
<guilabel>交点</guilabel>の設定は最初のものの範囲内に始まる、交差する格子を作成することによって、<guilabel>格子</guilabel>フィルタへもう一つの次元を追加します。
<!-- 
            The <guilabel>Intersection</guilabel> settings add another
            dimension to the <guilabel>Grid</guilabel> filter, by
            creating an intersecting grid that begins within the bounds
            of the primary one.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>幅</term>
        <listitem>
          <para>
<guilabel>幅</guilabel>は最初の格子の設定とほとんど同じように作用します。
<!-- 
            <guilabel>Width</guilabel> works much
            the same as the primary grid settings.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>間隔</term>
        <listitem>
          <para>
<guilabel>間隔</guilabel>は元の点から始まる交差する格子の距離に影響します。
<!-- 
            <guilabel>Spacing</guilabel> affects distance that the
            intersection grid starts from the point of origin.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>オフセット</term>
        <listitem>
          <para>
<guilabel>オフセット</guilabel>は描画した線の長さを拡張することで作用します。
<!-- 
            <guilabel>Offset</guilabel> works by extending the length of
            the rendered lines.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>単位</term>
        <listitem>
          <para>
最後のオプションのセットは<guibutton>単位</guibutton>ドロップダウンです。これは格子の線に対して異なる計測単位が反映するように調整できます。
<!-- 
            One final set of options are the
            <guibutton>Units</guibutton> dropdowns, which can be
            adjusted to reflect different measurement units for the grid
            lines.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>プレビュー更新</term>
        <listitem>
          <para>
このボタンはプレビューウィンドウを更新するので、格子の変更を最終描画の前に見ることができます。
<!-- 
            This button updates the preview window so any grid changes
            can be seen prior to the final render.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>色ボタン</term>
        <listitem>
          <para>
三つの色ボタンは<guilabel>水平</guilabel>、<guibutton>鉛直</guibutton>と、<guibutton>交差</guibutton>要素のための色を設定します。
<!-- 
            The three color wells set the color for
            <guilabel>Horizontal</guilabel>,
            <guibutton>Vertical</guibutton>, and
            <guibutton>Intersection</guibutton> elements.
 -->
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-burst">
  <title>破裂</title>
  <indexterm><primary>破裂</primary></indexterm>
  <?dbhtml filename="burst.html" dir="filters"?>

  <simplesect>
    <title>概要</title>
    
    <para>
<guilabel>破裂</guilabel>フィルタは現在選択しているブラシを使用して、放射状に一連の線を描画します。線は画像の中心から始まり、ユーザー指定の方法で外側に向かって発散します。スポークの色は<link linkend="toolbox-toolbox">道具箱</link>の前景色を使用してセットします。
<!-- 
      The <guilabel>Burst</guilabel> filter renders a series of lines
      in a radial fashion using the currently selected brush. The lines
      start in the center of the image and radiate outward in a
      user-defined manner. The color of the spokes is set using the
      foreground color in the<link linkend="toolbox-toolbox">Toolbox</link>.
 -->
    </para>
    
  </simplesect>


  <simplesect>
    <title>破裂の設定</title>
    
    <variablelist>
      <varlistentry><term>形状</term>
        <listitem>
          <para>
<guilabel>形状</guilabel>の欄は描画の最終的な全体形状を制御することができます。
<!-- 
            The <guilabel>Shape</guilabel> section allows control over
            the final overall shape of the render.
 -->
            <itemizedlist>
              <listitem>
                <para>            
<guibutton>矩形</guibutton>の設定は全体的な矩形形状がスポークによりまとめられるようにするためスポークを終わらせます。
<!-- 
                  A setting of <guibutton>Rectangle</guibutton> will
                  terminate the spokes so that an overall rectangular
                  shape is achieved by the spokes.
 -->
                </para>
              </listitem>
              <listitem>
                <para>
<guibutton>楕円形</guibutton>の設定は、最終的に楕円形になるようにスポークを描画します。
<!-- 
                  The <guibutton>Ellipse</guibutton> setting renders the
                  spokes so that the ends form a circular shape.
 -->
                </para>
              </listitem>
            </itemizedlist>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>フェード方向</term>
        <listitem>
          <para>
<guilabel>フェード方向</guilabel>はスポークがフェードしてくる方向を記述します。実際のフェードはブラシオプションで設定します。
<!-- 
            <guilabel>Fade dir</guilabel>describes the direction from
            which the spokes will fade. The actual fading is set in the
            Brush Options.
 -->
          </para>
          <itemizedlist>
            <listitem>
              <para>
<guibutton>内側</guibutton>は外側から内側へスポークを描画します。
<!-- 
                <guibutton>In</guibutton> renders the spokes from
                the outside inwards.
 -->
              </para>
            </listitem>
            <listitem>
              <para>
<guibutton>外側</guibutton>は内側から外側へスポークを描画します。
<!-- 
                <guibutton>Out</guibutton> renders the spokes
                from the inside outwards.
 -->
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>スポーク</term>
        <listitem>
          <para>
<guilabel>スポーク</guilabel>の設定は描画に使用するスポークの数の変更します。
<!-- 
            The <guilabel>Spokes</guilabel> setting changes the number of
            spokes that will be used in the render.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>内側のピクセル</term>
        <listitem>
          <para>
<guilabel>内側のピクセル</guilabel>の設定はスポークを描画しはじめる画像の中心からの距離を制御します。
<!-- 
            The <guilabel>Inside Pixels</guilabel> setting controls the
            distance from the center of the image from which the spokes will
            begin rendering.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>外側のピクセル</term>
        <listitem>
          <para>
<guilabel>外側のピクセル</guilabel>の設定はどのくらい遠くまでスポークが発散するかを制御します。
<!-- 
            The <guilabel>Outside Pixels</guilabel> setting controls how
            far the spokes will radiate.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>開始角</term>
        <listitem>
          <para>
<guilabel>開始角</guilabel>は最初にスポークが描画される角度を制御します。
<!-- 
            <guibutton>Start Angle</guibutton> controls the angle at
            which the first spoke will be rendered.
 -->
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>弧角</term>
        <listitem>
          <para>
<guilabel>弧角</guilabel>はスポークが旋回する角度の全体的な最大数を設定します。ここで 180 を設定すると円で利用可能な全角度の半分でスポークを描画します。
<!-- 
            <guibutton>Arc Angle</guibutton> sets the overall maximum
            number of degrees that the spokes will traverse. A setting
            of 180 here would render the spokes along half of the total
            degrees available in a circle.
 -->
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.0//EN">
-->

<sect1 id="paths-stroke-path">
  <title>パスをストローク描画</title>
  <indexterm><primary>パスをストローク描画</primary></indexterm>
  <?dbhtml filename="stroke_path.html" dir="paths"?>

  <para>
カレントブラシで現在アクティブなパスに沿ってストローク (線引き) します。ストロークラインは描画ツールの中心をパスの中心にして描画します。この機能は <inlinemediaobject><imageobject><imagedata fileref="../images/stroke_path.png"></imageobject></inlinemediaobject> アイコンをクリックしても同様に行えます。
<!-- 
    Stroke (draw a line) along the currently active path with the
    current painting tool. The stroked line is drawn with the center of
    the paint tool on the center of the path. This function is the
    same as clicking the
    <inlinemediaobject> 
      <imageobject> 
	<imagedata fileref="../images/stroke_path.png"> 
      </imageobject> 
    </inlinemediaobject>
    icon.
 -->
  </para>

  <para>
例えば、選んだ描画ツールが通常の <quote>絵筆</quote> であれば、その時これが線を作成するために使われるでしょう。もしこのツールが、グラデーションやフェードアウトを使用するようなオプションを何も持っていないなら、パスがストローク描画される時これが重んじられるでしょう。これは描画ツールが受け入れるあらゆるモードも適用します。
<!-- 
    For example, if the paint tool selected is the the normal
    <quote>Paintbrush</quote>, this will be used to create the
    line. If this tool has any options set, such as using a gradient or fading
    out, this will be honored when the path is stroked. This
    applies to any of the modes that the painting tool accepts.
 -->
  </para>

  <para>
パスをストロークする時には次の描画ツールが働きます。働かすためにその時アクティブなツールになっている必要があります。描画ツールが選択されていなければ、その時は "絵筆" ツールが使われます。
<!-- 
    The following paint tools work when stroking a path. They need to
    be the currently active tool to work.  If no paint tool is selected,
    then the "Paintbrush" tool is used.
 -->

    <itemizedlist>
      <listitem> 
        <para>
	  <link linkend="tools-pencil">鉛筆</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-paintbrush">絵筆</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-eraser">消しゴム</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-airbrush">エアブラシ</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-clone">スタンプ</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-convolve">色混ぜ</link>
        </para>
      </listitem>

      <listitem> 
        <para>
	  <link linkend="tools-dodgeburn">暗室</link>
        </para>
      </listitem>

      <listitem>
        <para>
	  <link linkend="tools-smudge">にじみ</link>
        </para>
      </listitem>
    </itemizedlist>
  </para>
</sect1>

<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="tools-eraser">
  <title>Eraser</title>
  <indexterm><primary>Eraser</primary></indexterm>
  <?dbhtml filename="eraser.html" dir="tools"?>
  
  <sect2 id="tools-eraser-overview">
    <title>Overview</title>
    <para>
      <inlinemediaobject>
        <imageobject>
	  <imagedata fileref="../images/tools/tool_eraser.png" format="png">
        </imageobject>
      </inlinemediaobject>
      The <guibutton>Eraser</guibutton> is used to remove blocks of
      color from the current layer, selection, or image. If the
      <guibutton>Eraser</guibutton> is used on on the
      <guibutton>Background</guibutton> layer, the eraser will remove
      color areas and replace them with the current background color. If
      used on a normal floating layer, the color will be replaced with
      transparency. The same rules apply to non-alpha images.
    </para>
  </sect2>

  <sect2 id="tools-eraser-options">
    <title>Eraser Tool Options</title>

    <variablelist><title>Eraser Settings</title>
      <varlistentry><term>Incremental</term>
        <listitem>
          <para>
            The <guibutton>Incremental</guibutton> checkbox actiavtes
            incremental paint mode for the tool. More information about
            incremental mode can be found in the
            <link linkend="glossary-incremental">glossary</link>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Pressure Sensitivity</term>
        <listitem>
          <para>
            The <guilabel>Pressure Sensitivity</guilabel> section sets
            the sensitivity levels for input devices that support this
            option.
          </para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>Hard Edge</term>
        <listitem>
          <para>
            By default the <guibutton>Eraser</guibutton> tool softens
            the edges of and area erased. The <guibutton>Hard
            Edge</guibutton> toggle changes this behavior. Any area
            erased while this option is active will be erased with no
            softening of the edge of the brush used.
          </para>
        </listitem>
      </varlistentry>
      
      <varlistentry><term>Anti Erase</term>
        <listitem>
          <para>
            The <guibutton>Anti Erase</guibutton> function of the
            <guibutton>Erase</guibutton> tool can un-erase areas of an
            image. This feature only works when used on images with an
            alpha channel.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>

    <simplesect>
      <title>Additional Information</title>
      
      <para>
        Default Keyboard Shortcut:
        <keycombo>
          <keycap>Shift</keycap>
          <keycap>E</keycap>
        </keycombo>
      </para>
     
      <para>
	Key modifiers:
	<itemizedlist>
	  <listitem>
	    <para>
	      <keycombo>
		<keycap>Ctrl</keycap>
	      </keycombo>
	      toggles between <guibutton>Anti Erase</guibutton> and normal
	      erase modes.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <keycombo>
		<keycap>Alt</keycap>
	      </keycombo>
	      is used to create straight lines.
	    </para>
	  </listitem>
	  <listitem>
	    <para>
	      <keycombo>
		<keycap>Alt</keycap>
		<keycap>Ctrl</keycap>
	      </keycombo>
	      is used to create straight lines that are constrained to 15
	      degree absolute angles.
	    </para>
	  </listitem>
	</itemizedlist>
      </para>
    </simplesect>
  </sect2>
</sect1>

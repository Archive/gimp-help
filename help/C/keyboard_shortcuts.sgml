<!--
<!doctype appendix public "-//OASIS//DTD DocBook V4.0//EN">
-->

<appendix id="keyboard-shortcuts">
  <title>Keyboard Shortcuts</title>
  <indexterm><primary>Keyboard Shortcuts</primary></indexterm>
  <indexterm><primary>Shortcuts</primary></indexterm>
  <?dbhtml filename="keyboard_shortcuts.html"?>

  <para>
    Keyboard shortcuts provide a fast way to access menu items in
    <application>GIMP</application>. These are the default keyboard
    shortcuts - you can change them by highlighting a menu item
    (hovering the cursor over it) and pressing the desired key
    combination. On some keyboards, the "Alt" key may be called the
    "Meta" key.
  </para>

  <para>
    <table frame=all>
      <title>Toolbox Functions</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm> 
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Airbrush</entry>
	<entry><keycap>A</keycap></entry>
	</row>

	<row>
	<entry>Bezier Select</entry>
	<entry><keycap>B</keycap></entry>
	</row>

	<row>
	<entry>Blend</entry>
	<entry><keycap>L</keycap></entry>
	</row>

	<row>
	<entry>Bucket Fill</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>B</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Clone</entry>
	<entry><keycap>C</keycap></entry>    
	</row>

	<row>
	<entry>Color Picker</entry>
	<entry><keycap>O</keycap></entry>
	</row>

	<row>
	<entry>Convolve</entry>
	<entry><keycap>V</keycap></entry>
	</row>

	<row>
	<entry>Crop and Resize</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>C</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Default Colors</entry>
	<entry><keycap>D</keycap></entry>
	</row>

	<row>
	<entry>Dodge and Burn</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>D</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Elliptical Select</entry>
	<entry><keycap>E</keycap></entry>
	</row>

	<row>
	<entry>Eraser</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>E</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Flip</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>F</keycap></keycombo>
	</entry>    
	</row>

	<row>
	<entry>Free Select</entry>
	<entry><keycap>F</keycap></entry>
	</row>

	<row>
	<entry>Fuzzy Select</entry>
	<entry><keycap>Z</keycap></entry>
	</row>

	<row>
	<entry>Ink</entry>
	<entry><keycap>K</keycap></entry>
	</row>

	<row>
	<entry>Intelligent Scissors</entry>
	<entry><keycap>I</keycap></entry>
	</row>

	<row>
	<entry>Magnify</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>M</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Move</entry>
	<entry><keycap>M</keycap></entry>
	</row>

	<row>
	<entry>Paintbrush</entry>
	<entry><keycap>P</keycap></entry>
	</row>
	    
	<row>
	<entry>Pencil</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>P</keycap></keycombo>
	</entry> 
	</row>
	    
	<row>
	<entry>Rectangular Select</entry>
	<entry><keycap>R</keycap></entry>
	</row>
	    
	<row>
	<entry>Smudge</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>S</keycap></keycombo>
	</entry>
	</row>
	    
	<row>
	<entry>Swap Colors</entry>
	<entry><keycap>X</keycap></entry>
	</row>

	<row>
	<entry>Text</entry>
	<entry><keycap>T</keycap></entry>
	</row>

	<row>
	<entry>Transform</entry>
	<entry>
	  <keycombo action=simul><keycap>Shift</keycap><keycap>T</keycap></keycombo>
	</entry>  
	</row>

	</tbody>
	</tgroup> 
      </table>
   </para>

   <para>
   <table frame=all>
      <title>File Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1> 
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Close</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>W</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>New</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>N</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Open</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>O</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Quit</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Q</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Save</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>S</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Edit Menu</title> 
	<tgroup cols=2 align=left colsep=1 rowsep=1> 
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Clear</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>K</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Copy</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>C</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Copy Named</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>C</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Cut</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>X</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Cut Named</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>X</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Fill with Foreground Color</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>,</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Fill with Background Color</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>.</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Paste</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>V</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Paste Named</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>V</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Redo</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>R</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Undo</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>View Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Info Window</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>I</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Navigation Window</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>N</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Shrink Wrap</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>E</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Toggle Guides</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>T</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Toggle Rulers</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>R</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Toggle Selection</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>T</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Toggle Statusbar</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>S</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Zoom In</entry>
	<entry>
	  <keycap>=</keycap>
	</entry>
	</row>

	<row>
	<entry>Zoom Out</entry>
	<entry>
	  <keycap>-</keycap>
	</entry>
	</row>

	<row>
	<entry>Zoom to Actual Size (1:1)</entry>
	<entry>
	  <keycap>1</keycap>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Select Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Select All</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>A</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Feather Selection</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>F</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Float Selection</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>L</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Invert Selection</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>I</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Select None</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>A</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Sharpen</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>H</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Layers Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Anchor Layer</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>H</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Merge Visible Layers</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>M</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Image Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Duplicate</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>D</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Offset</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>O</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Grayscale Mode</entry>
	<entry>
	  <keycombo action=simul><keycap>Alt</keycap><keycap>G</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Indexed Mode</entry>
	<entry>
	  <keycombo action=simul><keycap>Alt</keycap><keycap>I</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>RGB Mode</entry>
	<entry>
	  <keycombo action=simul><keycap>Alt</keycap><keycap>R</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Dialogs Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1> 
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm>
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Brushes</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>B</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Gradients</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>G</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Layers, Channels &amp; Paths</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>L</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Palette</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>P</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Patterns</entry>
	<entry>
	  <keycombo action=simul><keycap>Ctrl</keycap><keycap>Shift</keycap><keycap>P</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para>

   <para>
   <table frame=all>
      <title>Filters Menu</title> 
	<tgroup cols=2 colsep=1 rowsep=1>
	<colspec align=left colwidth=5cm>
	<colspec align=center colwidth=4cm
	<thead>
	<row> 
	<entry>Function</entry>
	<entry>Shortcut</entry> 
	</row>
	</thead>
	<tbody>

	<row>
	<entry>Reshow Last</entry>
	<entry>
	  <keycombo action=simul><keycap>Alt</keycap><keycap>Shift</keycap><keycap>F</keycap></keycombo>
	</entry>
	</row>

	<row>
	<entry>Repeat Last</entry>
	<entry>
	  <keycombo action=simul><keycap>Alt</keycap><keycap>F</keycap></keycombo>
	</entry>
	</row>

	</tbody>
	</tgroup>
      </table>
   </para
</appendix>

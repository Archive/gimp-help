#!/bin/bash
cp --parents stylesheet-images/*.* gimp 
cp --parents images/*.* gimp
cp --parents images/tools/*.* gimp
cp --parents images/glossary/*.* gimp
cp --parents images/examples/*.* gimp
cp --parents images-C/*.* gimp

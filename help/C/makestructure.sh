#!/bin/bash

cd gimp
cd channels
ln -s ../dialogs/channels dialogs
cd ..
cd file
ln -s ../dialogs dialogs
ln -s ../filters filters
ln -s ../open open
ln -s ../save save
cd ..
cd image
ln -s ../dialogs dialogs
ln -s ../file file
ln -s ../filters filters
ln -s ../layers layers
ln -s ../toolbox toolbox
ln -s ../tools tools
cd edit
ln -s ../../dialogs dialogs
cd ..
cd image
ln -s ../../dialogs dialogs
cd mode
ln -s ../../../dialogs dialogs
cd ..
cd transforms
ln -s ../../../dialogs dialogs
cd ../..
cd select
ln -s ../../dialogs dialogs
cd ..
cd view
ln -s ../../dialogs dialogs
cd ../..
cd layers
ln -s ../dialogs/layers dialogs
cd ..
cd open
ln -s ../dialogs dialogs
ln -s ../filters filters
cd ..
cd paths
ln -s ../dialogs/paths dialogs
ln -s ../filters filters
cd ..
cd save
ln -s ../dialogs dialogs
ln -s ../filters filters
cd ..
cd toolbox
ln -s ../dialogs dialogs
ln -s ../file file
ln -s ../filters filters
cd help
ln -s ../../dialogs dialogs
ln -s ../../filters filters

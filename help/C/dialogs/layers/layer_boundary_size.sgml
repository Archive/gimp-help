<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="dialogs-layers-layer-boundary-size">
  <title>Layer Boundary Size</title>
  <indexterm><primary>Layer Boundary Size</primary></indexterm>
  <?dbhtml filename="layer_boundary_size.html" dir="dialogs/layers"?>

  <para>
    The Layer Boundary Size lets you set the the boundary of the
    layer. Remember you can have a smaller or larger layer than the
    image boundary size. When you enlarge the boundary size, then
    you will add some space to paint on to your layer. Naturally it
    will be vice versa when you make the boundary size smaller.
  </para>

  <para>
    The <guibutton>Size</guibutton> area will control how big or
    small your new layer boundary will be just as when you created a
    <link linkend="dialogs-layers-new-layer">New Layer</link>. The
    <guibutton>Offset</guibutton> area will control how your layer
    is clipped or expanded.
  </para>

  <para>
    The <guibutton>Offset</guibutton> fields will control where your
    upper left corner of the old layer will be in the
    <quote>new</quote> layer. The best way to control the new location
    and how and where the layer will be clipped is to drag the
    <quote>layer preview</quote> in the <guibutton>Offset</guibutton>
    area to the right position. Make the final adjustment with the
    spin buttons (or type it in by hand) if it needs to be
    pixel-exact. The thin outline is the new layer size and you have
    to drag the old layer to the right position so it will be clipped
    according to your demands. If you make the layer larger then the
    outline/canvas is the new layer size and you have to drag the old
    layer to the right position within it.
  </para>

  <sect2>
  <title>The Chain Button</title>

  <para>
      If you uncheck the chain in the "Size" area, you will be able to
      have different ratios when you make the "new" layer . It's
      therefore possible e.g. to have a layer which is smaller in X
      direction and larger in Y direction than before you altered the
      layer boundary size.
  </para>
  </sect2>
</sect1>


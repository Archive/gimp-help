<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="dialogs-export-file">
  <title>Export File</title>
  <indexterm>
    <primary>Export File</primary>
    <secondary>Indexed Mode</secondary>
  </indexterm>
  <?dbhtml filename="export_file.html" dir="dialogs"?>

  <para>
    If an attempt is made to save an <acronym>RGB</acronym> image as a
    filetype that does not support more than 256 colors, this dialog window
    will be presented to the user. Each filetype will have a slightly
    different dialog layout, but the dialog clearly illustrates the action
    the user must take to rectify the situation. The export options for
    <acronym>GIF</acronym> export are shown below.
  </para>

  <note>
    <para>
      Exporting a file will not modify the original image in any way.
    </para>
  </note>
  
  <simplesect id="exportfile-gif">
    <title>Exporting to GIF</title>
    <variablelist>
      <title>GIF can only handle layers as animation frames</title>
      <varlistentry>
        <term>Merge Visible Layers</term>
        <listitem>
          <para>
            When exporting to GIF, any layers contained in the image may be
            used as frames in an animation. If this is not the desired
            result, this option should be enabled.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Save as Animation</term>
        <listitem>
          <para>
            If the image is being saved as an animation, this option should
            be chosen.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
    <variablelist>
      <title>GIF can only handle grayscale or indexed images</title>
      <varlistentry>
        <term>Convert to indexed using default settings</term>
        <listitem>
          <para>
            This option should be used when saving the image as a color GIF.
            Manual conversion can be achieved by using the
            <link linkend="dialogs-convert-to-indexed">
              Convert to Indexed
            </link>
            feature.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Convert to Grayscale</term>
        <listitem>
          <para>
            If this option is chosen, the image will be converted to a
            grayscale indexed image before saving.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

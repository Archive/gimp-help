<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.0//EN"[]>
-->

<sect1 id="image-image-mode-grayscale">
  <title>Convert to Grayscale</title>
  <indexterm><primary>Convert to Grayscale</primary></indexterm>
  <?dbhtml filename="convert_to_grayscale.html" dir="image/image/mode"?>

  <para>
    Converting images to grayscale is not unlike the effect produced by using
    the
    <link linkend="image-image-colors-desaturate">
    <guimenuitem>Desaturate</guimenuitem>
    </link>
    function. The primary difference is that converting an image to grayscale
    results in the image having the color channels removed. Thus, an image
    that has been converted to grayscale cannot have color added to it.
  </para>
  
  <simplesect>
    <title>Additional Information</title>
  
    <para>
      Default Keyboard Shortcut:
      <keycombo>
        <keycap>Alt</keycap>
        <keycap>G</keycap>
      </keycombo>
    </para>
  </simplesect>
</sect1>

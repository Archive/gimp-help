<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]> 
-->

<sect1 id="why-gimp"> 
  <title>
    What Can The <application>GIMP</application> Do For Me?
  </title>
  <indexterm><primary>Why GIMP</primary></indexterm>
  <?dbhtml filename="why_gimp.html"?>

  <para>
    The <application>GIMP</application> is a very powerful application
    which has many uses.
  </para>

  <sect2>
    <title>Image Editing</title>
    <para>
      The <application>GIMP</application>'s main use is for the creation
      and editing of
      <link linkend="glossary-bitmap">
        bitmap
      </link>
      images. This ranges from the touching up of digital photographs to
      the creation of digital art or the authoring of logos.
    </para>

    <para>
      The other common method of handling images is by using vectors. A
      vector image is made up of primitives such as lines, curves,
      circles, rectangles and fills. The <application>GIMP</application>
      has some support for vector drawing in the
      <application>Gfig</application> plug-in, and the
      <link linkend="dialogs-paths-paths">
        path tool
      </link>,
      but these do not provide a complete editing environment and are
      not well suited to the creation of complex vector diagrams.
    </para>
  </sect2>

  <sect2>
    <title>Video Editing</title>
      <para>
        The <application>GIMP</application> also offers some image
        editing features, which are mainly useful for creating small
        animations because the editing is done on a frame-by-frame
        basis. The <application>GIMP</application> supports reading and
        writing <acronym>AVI</acronym> and <acronym>GIF</acronym>
        animation formats and can also read <acronym>MPEG</acronym>
        videos.
    </para>
  </sect2>
</sect1>

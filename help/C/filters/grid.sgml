<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-grid">
  <title>Grid</title>
  <indexterm><primary>Grid</primary></indexterm>
  <?dbhtml filename="grid.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      The <guilabel>Grid</guilabel> filter places a custom grid over the
      image. All aspects of the grid can be customized.
    </para>
  </simplesect>

  <simplesect>
    <title>Grid Options</title>
    
    <variablelist><title>Grid Settings</title>
      <varlistentry><term>Horizontal / Vertical</term>
        <listitem>
          <para>
            The <guilabel>Horizontal</guilabel>, and
            <guilabel>Vertical</guilabel> settings are used to adjust,
            <guilabel>Width</guilabel>, <guilabel>Spacing</guilabel>,
            and <guilabel>Offset</guilabel>. <guilabel>Width</guilabel>
            sets the line width when rendering the grid.
            <guilabel>Spacing</guilabel> affects the distance between
            the grid lines. <guilabel>Offset</guilabel> determines the
            offset from the top-left corner of the image, from which
            rendering will begin. Located below the
            <guilabel>Offset</guilabel> settings, are located the
            <guilabel>Color Wells</guilabel>, which set the colors for
            the grid lines. All of these settings can be
            <quote>Locked</quote> together to balance the
            <guilabel>Horizontal</guilabel>, and the
            <guilabel>Vertical</guilabel> grid lines, or can be
            <quote>Unlocked</quote>, to provide more control.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Intersection</term>
        <listitem>
          <para>
            The <guilabel>Intersection</guilabel> settings add another
            dimension to the <guilabel>Grid</guilabel> filter, by
            creating an intersecting grid that begins within the bounds
            of the primary one.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Width</term>
        <listitem>
          <para>
            <guilabel>Width</guilabel> works much
            the same as the primary grid settings.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Spacing</term>
        <listitem>
          <para>
            <guilabel>Spacing</guilabel> affects distance that the
            intersection grid starts from the point of origin.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Offset</term>
        <listitem>
          <para>
            <guilabel>Offset</guilabel> works by extending the length of
            the rendered lines.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Units</term>
        <listitem>
          <para>
            One final set of options are the
            <guibutton>Units</guibutton> dropdowns, which can be
            adjusted to reflect different measurement units for the grid
            lines.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Update Preview</term>
        <listitem>
          <para>
            This button updates the preview window so any grid changes
            can be seen prior to the final render.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Color Wells</term>
        <listitem>
          <para>
            The three color wells set the color for
            <guilabel>Horizontal</guilabel>,
            <guibutton>Vertical</guibutton>, and
            <guibutton>Intersection</guibutton> elements.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

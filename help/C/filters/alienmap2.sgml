<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-alienmap2">
  <title>Alien Map 2</title>
  <indexterm><primary>Alien Map 2</primary></indexterm>
  <?dbhtml filename="alienmap2.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      The Alien Map 2 filter is a color manipulation filter based on the
      <link linkend="filters-alienmap">
        Alien Map
      </link>
			filter. Alien Map 2 is much more advanced in that it can perform
			calculations based not only on the <acronym>RGB</acronym>
			properties of the active image or layer, but also the
			<acronym>HSL</acronym> properties of the active image.  More
			control of the math involved is possible, allowing the user to
			adjust the <guibutton>Frequency</guibutton> and the
			<guibutton>Phaseshift</guibutton> values which can yield more
			profound transformations.
    </para>
  </simplesect>
    
  <simplesect>
    <title>Alien Map 2 Options</title>

    <variablelist><title>Alien Map 2 Settings</title>
      <varlistentry><term>Mode</term>
        <listitem>
          <para>
            There are two available options for <guilabel>Mode</guilabel>.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>RGB Color Model</guibutton>: This option will
								allow <guilabel>Alien Map 2</guilabel> to work with Red,
                Green, and Blue colors only.
              </para>
            </listitem>
            <listitem>
              <para>
								<guibutton>HSL Color Model</guibutton>: This option will
								allow <guilabel>Alien Map 2</guilabel> to work within
								the Hue, Saturation, and Luminance of the image, layer,
								or selection.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Modify Red/Hue Channel</term>
        <listitem>
          <para>
            This checkbox allows modifications to be made to the Red or
            Hue of the active selection or layer.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Modify Green/Saturation Channel</term>
        <listitem>
          <para>
            This checkbox allows modifications to be made to the Green
            or Saturation of the active selection or layer.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Modify Blue/Luminance Channel</term>
        <listitem>
          <para>
            This checkbox allows modifications to be made to the Blue or
            Luminance of the active selection or layer.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Frequency Sliders</term>
        <listitem>
          <para>
            The <guilabel>Frequency</guilabel> sliders control changes
            to the applicable channel. The slider determines the
            frequency intensity at which the colors will be mapped.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Phaseshift Sliders</term>
        <listitem>
          <para>
            The <guilabel>Phaseshift</guilabel> sliders control changes
            to the applicable channel. The slider determines the shift
            in frequency that will be used to map the channel colors.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
  
  <simplesect>
    <title>See also</title>

    <para>
      Further information can be found in the glossary regarding:
      <link linkend="glossary-channels">
        Channels
      </link>
      and
      <link linkend="glossary-hsl">
        HSL
      </link>
    </para>
  </simplesect>    
</sect1>

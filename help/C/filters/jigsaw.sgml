<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-jigsaw">
  <title>Jigsaw</title>
  <indexterm><primary>Jigsaw</primary></indexterm>
  <?dbhtml filename="jigsaw.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      The <guilabel>Jigsaw</guilabel> filter renders a simple jigsaw
      pattern to the current layer.
    </para>
  </simplesect>

  <simplesect>
    <title>Jigsaw Options</title>
    
    <variablelist><title>Jigsaw Settings</title>
      <varlistentry><term>Number of Tiles</term>
        <listitem>
          <para>
           The <guilabel>Number of Tiles</guilabel> settings control
           both the number of <guibutton>Horizontal</guibutton> tiles,
           and the number of <guibutton>Vertical</guibutton> tiles.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Bevel Edges</term>
        <listitem>
          <para>
            The <guilabel>Bevel Edges</guilabel> settings allow control
            over the rendering of the edges of the tiles.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guilabel>Bevel Width</guilabel>: This slider sets the
                width of the shading on each tile.
              </para>
            </listitem>
            <listitem>
              <para>
                <guilabel>Highlight</guilabel>: This slider  controls
                the lighting aggressiveness. A higher
                <guilabel>Highlight</guilabel> setting, will produce
                tiles with more light.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Jigsaw Style</term>
        <listitem>
          <para>
            <guilabel>Jigsaw Style</guilabel> sets the type of jigsaw
            cuts that will be rendered.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Square</guibutton>: This sets the cuts to the
                traditional squared type of jigsaw puzzle cuts.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Curved</guibutton>: This sets the cuts to be
                made in the more contemporary rounded cut.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Disable Tooltips</term>
        <listitem>
          <para>
            There are floating tooltips over all sliders in this plug-in.
            The display of these tooltips can be turned on or off with
            this checkbox.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

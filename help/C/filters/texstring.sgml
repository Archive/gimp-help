<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-texstring">
  <title>TeX String</title>
  <indexterm><primary>TeX String</primary></indexterm>
  <indexterm><primary>TEX</primary></indexterm>
  <?dbhtml filename="tex-to-float.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      The <guilabel>TeX String</guilabel> filter renders a TeX string as
      a new floating layer. For more information on TeX, visit <ulink
      url="http://www.ctan.org/">the Comprehensive TeX Archive
      Network</ulink>.
    </para>
  </simplesect>

  <simplesect>
    <title>TeX String Options</title>

    <variablelist><title>TeX String Settings</title>
      <varlistentry><term>Input File</term>
        <listitem>
          <para>
            <guilabel>Input file</guilabel> is where the name of the TeX
            macro file should be inserted.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>TeX string</term>
        <listitem>
          <para>
            <guilabel>Tex string</guilabel> is where the actual TeX
            String should be input.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Dpi</term>
        <listitem>
          <para>
            <guilabel>Dpi</guilabel> sets the rendering
            <acronym>DPI</acronym> level.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Magstep</term>
        <listitem>
          <para>
            <guilabel>Magstep</guilabel> sets the TeX magstep.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Anti aliasing</term>
        <listitem>
          <para>
            <link linkend="glossary-anti-aliasing">
              <guilabel>Anti aliasing</guilabel>
            </link> controls the level of
            antialiasing that will be applied to the string at
            render time.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

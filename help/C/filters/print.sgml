<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN"[]>
-->

<sect1 id="filters-print">
  <title>The Print Plug-In</title>
  <indexterm><primary>Print</primary></indexterm>
  <?dbhtml filename="print.html" dir="filters"?>

  <figure id="filters-print-main-png">
    <title>The Main Print Dialog</title>
    <mediaobject>
      <imageobject>
	<imagedata fileref="../images/print_main.png" format="png">
      </imageobject>
    </mediaobject>
  </figure>
    
  <para>
    The main window is divided into five panes:
    <variablelist>
      <varlistentry><term><guilabel>Preview</guilabel></term>
        <listitem>
      	  <para>
	    The <guilabel>Preview</guilabel> pane contains a
	    <guibutton>positioning widget</guibutton> that allows
	    interactive positioning of the output on the page. It contains
	    an outer border, representing the sheet of paper; an inner
	    border, representing the printable area of the printer; an
	    arrow, pointing to the top of the page (the end that is fed
	    into the printer); and a black rectangle, representing the
	    position of the image on the page. The image can be moved
	    around on the paper. When the left
	    <mousebutton>left</mousebutton> mousebutton is used, the image
	    is moved in screen pixels; when any other button is used, the
	    image is moved in points
	    <footnote>
	      <para>
	        the output resolution of the plug-in
	      </para>
	    </footnote>. The arrow resizes depending
	    upon the media size chosen; the shaft of the arrow is always
	    equal to one inch on the output.
      	  </para>
        </listitem>
      </varlistentry>
      <varlistentry><term><guilabel>Printer Settings</guilabel></term>
        <listitem>
	  <para>
	    The <guilabel>Printer Settings</guilabel> pane contains a
	    dropdown menu for selecting a printer. There is a special
	    <quote>printer</quote> named File that allows you to choose a
	    file to print to, rather than a printer queue. The
	    <guilabel>Setup</guilabel> box to the right allows
	    specification of a printer type, a PPD file
	    <footnote><para>for Postscript printers</para></footnote>,
	    and the command to be used to print. Each distinct printer in
	    the <guibutton>Printer</guibutton> list can have different
	    settings applied to it. Below that is a <guibutton>combo
	    box</guibutton> allowing choice of media size. The choices are
	    constrained to those that the printer supports.  Below that are
	    <guibutton>dropdown</guibutton> menus for choosing media type
	    (what kind of paper), media source (what input tray), ink type,
	    and resolution.  All of these settings are printer-specific.
	  </para>
	  
	  <figure id="filters-print-setup-png">
	    <title>The Printer Setup Dialog</title>
	    <mediaobject>
	      <imageobject>
		<imagedata fileref="../images/print_setup.png" format="png">
	      </imageobject>
	    </mediaobject>
	  </figure>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term><guilabel>Position</guilabel></term>
        <listitem>
	  <para>
	    The <guilabel>Position</guilabel> pane contains various
	    widgets to place the image on the paper. These widgets
	    work in conjunction with the <guilabel>Preview</guilabel>
	    pane. At the top left of the pane is a button to center
	    the image on the paper (not on the printable area). To its
	    right is a <guibutton>button group</guibutton> that allows
	    choosing English (inch) units or metric (centimeter)
	    units. Below these are <guibutton>four boxes</guibutton>
	    that allow entry of the left, top, right, and bottom of
	    the image. These positions are relative to the top left of
	    the paper
	    <footnote>
	      <para>
		again, that's relative to the paper corner, not the
		printable area, which is usually smaller
	      </para>
	    </footnote>.  There are two additional boxes
	    that allow specification of the right margin and bottom
	    margin if you prefer; these are relative to the bottom
	    right corner of the paper. Any of these may have values
	    entered into them. The preview image will be moved
	    appropriately. 
	    <note>
	      <para>
	        These entries do not resize the image.
	      </para>
	    </note> 
	    Finally, there is a pick box for
	    <guibutton>orientation</guibutton> (landscape or portrait).
	    There is an <guibutton>Auto</guibutton> mode that picks the
	    orientation that best matches that of the image to be printed.
	  </para>
        </listitem>
      </varlistentry>

      <varlistentry>
	<term><guilabel>Scaling</guilabel></term>  
        <listitem>
	  <para>
	    The <guilabel>Scaling</guilabel> pane contains a slider
	    that allows scaling of the image. The image can be scaled
	    in either percent of the printable area (NOT the page in
	    this case) or pixels per inch (<acronym>PPI</acronym>) via
	    a <guibutton>radio button</guibutton> below the slider.
	    <acronym>PPI</acronym> allows matching image resolution to
	    printer resolution.  The image may be scaled using either
	    method to between 5 and 100&percnt; of the imageable area. It is
	    not possible to crop with the Print plugin. In
	    <guibutton>Percent</guibutton> mode, the image is scaled
	    so that neither axis will be longer than the percent of
	    the printable area specified. For example, if you print an
	    image at 20&percnt;, it will be possible to tile the image 5
	    times on one axis and at least 5 times on the other. To
	    the right of the radio button is a button called
	    <guibutton>Set Image Scale</guibutton>. This sets the
	    scaling to <acronym>PPI</acronym>, and sets the resolution
	    as closely as possible to the resolution stored in the
	    image. To the right of the <guibutton>Set Image
	    Scale</guibutton> button are two boxes that allow entry of
	    <guibutton>width</guibutton> and
	    <guibutton>height</guibutton> of the image. These set the
	    scaling mode to <acronym>PPI</acronym>.  Specifying one
	    automatically sets the other, and the image is
	    repositioned as needed to prevent it from falling off the
	    edge of the page.
	  </para>
	</listitem>
      </varlistentry>
	
      <varlistentry>
	<term><guilabel>Image Settings</guilabel></term>
	<listitem>
	  <para>
	    The <guilabel>Image Settings</guilabel> pane allows choice
	    of <guibutton>Line Art</guibutton>, <guibutton>Solid
	    Colors</guibutton>, <guibutton>Photograph</guibutton>, or
	    <guibutton>Monochrome</guibutton> image type. Line art or
	    Solid Colors should be used for graphics containing mostly
	    solid areas of color. They're very similar to each other.
	    Photograph mode dithers more slowly, but produces more
	    accurate colors. Finally, Monochrome mode can be used to
	    print absolute black and white very quickly. To the right
	    of these four radio buttons is a button called
	    <guibutton>Adjust Color</guibutton>. This pops up a new
	    window that controls various output quality settings. That
	    will be described separately. Finally, there is a choice
	    of Black and White and Color output.
	  </para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
	<term><guilabel>Action Buttons</guilabel></term>
	<listitem>
	  <para>
	    The last pane contains four action buttons:

	    <itemizedlist> 
	      <listitem>
	        <para>
		  <guibutton>Print and Save Settings</guibutton>
		  &mdash; immediately print the image (or, if the File
		  printer is chosen, display a file selection window to
		  pick the output file), and save all current settings
		  for all printers.
	        </para>
	      </listitem>
	     
	      <listitem>
	        <para>
		  <guibutton>Save Settings</guibutton> &mdash;
		  immediately save the settings, and continue working
		  in the Print plugin.
  	        </para>
	      </listitem>
	     
	      <listitem>
	        <para>
		  <guibutton>Print</guibutton> &mdash; immediately
		  print the image (or, if the File printer is chosen,
		  display a file selection window to pick the output
		  file), but do not save settings.
 	        </para>
	      </listitem>
	     
	      <listitem>
	        <para>
		  <guibutton>Cancel</guibutton> &mdash; immediately
		  quits without saving or printing.
 	        </para>
	      </listitem>
	    </itemizedlist> 
	  </para>
        </listitem>
      </varlistentry>
    </variablelist> 
  </para>
  
  <simplesect>
    <title>Adjust Color</title>
    <para>
      The <guibutton>Adjust Color</guibutton> button pops up a
      non-modal dialog that
      allows adjustment of various parameters related to the print
      quality. These are independent of the controls within the
      <application>GIMP</application> itself and only affect the
      print.
    </para>

    <figure id="filters-print-color-png">
      <title>The Printer Color Settings Dialog</title>
      <mediaobject>
	<imageobject>
	  <imagedata fileref="../images/print_color.png" format="png">
	</imageobject>
      </mediaobject>
    </figure>
 
    <para>
      At the top of the window is a thumbnail of the image that
      changes to reflect the color settings of the image.  This
      enables you to get an idea of how the image will print out as
      you adjust settings.
    </para> 
    
    <para>
      Below that there are eight sliders:
      
      <variablelist>
	<varlistentry>
	  <term><guilabel>Brightness (0-2.0, default 1.0)</guilabel></term>
	  <listitem>
	    <para>
	      adjust the brightness of the image.
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Contrast (0-4.0, default 1.0)</guilabel></term>
	  <listitem>
	    <para>
	      adjust the output contrast.
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Cyan, Magenta, Yellow (0-4.0, default 1.0)
	    </guilabel></term>
	  <listitem>
	    <para>
	      adjust the cyan, magenta, and yellow in the output.
	      These should not normally need to be adjusted very much;
	      even very small adjustments can go quite a long way to
	      restoring color balance..
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Saturation (0-9.0, default 1.0)
	    </guilabel></term>
	  <listitem>
	    <para>
	      adjust the color brilliance (saturation) of the output.
	      Saturation of 0 means pure gray scale, with no color.
	      Saturation of 9.0 will make just about anything but
	      pure grays brilliantly colored.
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Density (0.1-2.0, default 1.0)
	    </guilabel></term>
	  <listitem>
	    <para>
	      adjust the density (amount of ink) in the print. The
	      density is automatically corrected for the particular
	      printer, resolution, and, in some cases, paper choices.
	      If solid black in the input is not solid in the print,
	      the density needs to be increased; if there is excessive
	      ink bleed-through and muddy dark colors, the density
	      should be decreased. <note><para>The density will not
	      increase beyond a certain amount no matter what the
	      slider is set to.</para></note>
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Gamma (0.1-4.0, default 1.0)
	    </guilabel></term>
	  <listitem>
	    <para>
	      adjust the output gamma. The gamma value is
	      automatically corrected for the choice of printer; this
	      is used if you believe the automatic setting is
	      incorrect.
	    </para>
	  </listitem>
	</varlistentry>
	  
	<varlistentry>
	  <term><guilabel>Dither Algorithm</guilabel></term>
	  <listitem>
	    <para>
	      There is also a selection box for the <guilabel>dither
	      algorithm</guilabel> to be used. There are currently
	      seven choices:
	      <itemizedlist>
	        <listitem>
		  <para>
		    <guilabel>Adaptive Hybrid</guilabel> usually
		    yields the best output
		    quality.  It chooses a modified Floyd-Steinberg
		    error diffusion algorithm or ordered dithering
		    depending upon the image characteristics.
		  </para>
	        </listitem>

	        <listitem>
		  <para>
		    <guilabel>Ordered</guilabel> uses a pure ordered
		    dither. It generally
		    yields excellent quality for simple black and
		    white or four color printers without variable drop
		    size or drop modulation.  It is not recommended if
		    high quality is desired on six color printers.  It
		    is considerably faster than <guilabel>Adaptive
		    Hybrid</guilabel>. 
		  </para>
	        </listitem>
		    
	        <listitem>
		  <para>
		    <guilabel>Fast</guilabel> also uses a pure ordered
		    dither, but uses a
		    very simple black model and makes no attempt to
		    handle multi-level (6-color, variable drop size,
		    or drop modulation) at all cleanly. It is
		    substantially faster than
		    <guilabel>Ordered</guilabel> dither. The
		    quality tends to be quite poor except on simple
		    four color printers. On three color printers,
		    quality is probably competitive with anything
		    else.
		  </para>
	        </listitem>
		    
	        <listitem>
		  <para>
		   <guilabel>Very Fast</guilabel> is similar to
		   <guilabel>Fast</guilabel>, except that it uses a
		   very simple dither matrix that can be looked up
		   much more quickly than the matrix used in the
		   <guilabel>Fast dither</guilabel>.  For simple pure
		   black and white images dominated by horizontal and
		   vertical lines, this may actually yield the best
		   results.  For other types of images, the quality
		   will be poor.
		  </para>
	        </listitem>
		    
	        <listitem>
		  <para>
		    <guilabel>Adaptive Random</guilabel> is similar to
		    <guilabel>Adaptive Hybrid</guilabel>,
		    except that the modifications to the
		    Floyd-Steinberg algorithm are slightly different.
		    This is slower than <guilabel>Adaptive
		    Hybrid</guilabel> on most
		    systems.  For some images the quality may be
		    better, but generally
		    <guilabel>Adaptive Hybrid</guilabel> should yield
		    slightly superior
		    images.
		  </para>
	        </listitem>
		    
	        <listitem>
		  <para>
		    <guilabel>Hybrid Floyd-Steinberg</guilabel> uses
		    the modified
		    Floyd-Steinberg algorithm of <guilabel>Adaptive
		    Hybrid</guilabel> on
		    the entire image.  Generally, the results are poor
		    in pale regions.
		  </para>
	        </listitem>
		    
	        <listitem>
		  <para>
		    <guilabel>Random Floyd-Steinberg</guilabel> uses
		    the modified Floyd-Steinberg algorithm of
		    <guilabel>Adaptive Random</guilabel> on
		    the entire image. Generally, the results are poor
		    in pale regions.
		  </para>
	        </listitem>
	      </itemizedlist>
            </para>
          </listitem>
        </varlistentry>
      </variablelist> 
    </para> 
  </simplesect>
</sect1>


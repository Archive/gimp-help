<!--
<!doctype sect1 public "-//OASIS//DTD DocBook V4.1//EN">
-->

<sect1 id="filters-tiff">
  <title>TIFF</title>
  <indexterm><primary>TIFF</primary></indexterm>
  <?dbhtml filename="tiff.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      Allows you to configure settings for saving a
      <acronym>TIFF</acronym> (Tagged Image File Format) image.
    </para>

    <para>
      <acronym>TIFF</acronym> files can be compressed in a number of
      ways to reduce the file size. <acronym>LZW</acronym> (Lempel Ziv
      Welch) compression is the same method used in
      <acronym>GIF</acronym> images, and produces a significant
      reduction in file size without losing any image information (it is
      a lossless method). It should be noted that <acronym>LZW</acronym>
      compression is covered by a restrictive Unisys license.
    </para>

    <para>
      PackBits compression is often used on Macintosh systems and is a
      form of Run Length Encoding (<acronym>RLE</acronym>) which, like
      <acronym>LZW</acronym>, is a lossless method.
    </para>

    <para>
      Deflate compression, sometime known as "zip" compression, uses
      another variant of the <acronym>LZW</acronym> compression method
      and so gives similar results, but is not restricted by any
      licenses. <acronym>LZW</acronym> compression should be used on
      logo-type images such as maps and line diagrams.
    </para>

    <para>
      <acronym>JPEG</acronym> compression is the same compression
      methods used when saving <acronym>JPEG</acronym>
      files. <acronym>JPEG</acronym> compression should be used on
      photographs or realistic scenes.
    </para>

    <para>
      Finally, you can leave the <acronym>TIFF</acronym> uncompressed
      for maximum quality, but be warned that file sizes can be very
      large.
    </para>

    <para>
      As with <acronym>JPEG</acronym> (and some other formats) a comment
      can be added to the image up to 32KB. A default comment for new
      images can be set in <guimenuitem>Preferences</guimenuitem>.
    </para>
  </simplesect>
</sect1>

<!--<!doctype sect1 public "-//OASIS//DTD DocBook V4.0//EN">-->

<sect1 id="filters-gfig">
  <title>Gfig</title>
  <indexterm><primary>Gfig</primary></indexterm>
  <?dbhtml filename="gfig.html" dir="filters"?>

  <simplesect>
    <title>Overview</title>
    
    <para>
      The <guilabel>Gfig</guilabel> plug-in is a versatile plug-in that is
      used to create geometric shapes. It makes use of definable paths and
      the currently selected brush.
      The <guilabel>Gfig</guilabel> dialog window is neatly divided into
      three areas.
    </para>
  </simplesect>
  
  <simplesect>
    <title>Gfig Options</title>
    
    <itemizedlist>
      <listitem>
        <para>
          <guilabel>Ops</guilabel> contains the shape creation tools.
          These are the tools that are used to control the shapes that
          are created with <guilabel>Gfig</guilabel>.
        </para>
      </listitem>
      <listitem>
        <para>
          <guilabel>Preview</guilabel> contains the shape preview area,
          the <guilabel>Object Details</guilabel>, and the
          <guilabel>Collection Details</guilabel>. The main preview
          window is where the creation of shapes will occur.
        </para>
      </listitem>
      <listitem>
        <para>
          <guilabel>Settings</guilabel> shows many control options. This
          area includes save and delete tools for shapes. Also a
          mini-preview can be seen. <guilabel>Grid</guilabel> settings
          are found here, as well as the rendering options.
        </para>
      </listitem>
    </itemizedlist>
  </simplesect>
  
  <simplesect>
    <title>Object Settings</title>
    <para>
      The <guilabel>Object</guilabel> section, inside
      <guilabel>Settings</guilabel>, offers the loading, creation, and
      saving features of the plug-in. 
    </para>
    <variablelist>
      <varlistentry><term>Rescan</term>
        <listitem>
          <para>
            This allows the rescanning of the list of available shapes
            from the file system. Any shapes that are saved to the
            file system can be loaded into the list via this button.
            Clicking the <guibutton>Rescan</guibutton> button will
            display a dialog that shows the currently loaded shape
            paths. New paths can be added by clicking the icon in the
            top left.
            A browse button is provided in the top right of the dialog.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Load</term>
        <listitem>
          <para>
            The <guibutton>Load</guibutton> button is used for loading
            individual shapes into the editor. If there is a Gfig file
            located somewhere on the file system, this button is used to
            load the shape.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>New</term>
        <listitem>
          <para>
            Clicking this button will remove all of the
            current shape settings and create a new shape file. After
            clicking the <guibutton>New</guibutton> button, a dialog
            will request a filename for the new shape.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Delete</term>
        <listitem>
          <para>
            <guibutton>Delete</guibutton> is the button used to delete a
            saved shape.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Edit</term>
        <listitem>
          <para>
            The <guibutton>Edit</guibutton> button is used to transfer
            the currently displayed small preview into the main preview
            window.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Merge</term>
        <listitem>
          <para>
            The <guibutton>Merge</guibutton> button is used to merge the
            current shape with the shape in the main preview window.
            This function adds the new shape to the current one.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
    <para>
      Upon editing an existing shape or creating a new shape, a disk
      icon will appear next to the name of the shape. This icon
      indicates that the shape has not yet been written to disk. A red
      cross indicates that the file is read-only and cannot be saved.
    </para>
  </simplesect>  

  <simplesect>
    <title>Grid Options</title>
    <para>
      The <guilabel>Grid</guilabel> settings located below the
      <guilabel>Object</guilabel> settings control the editing grid
      layout and behavior.
    </para>
    <variablelist>
      <varlistentry><term>Snap to Grid</term>
        <listitem>
          <para>
            This checkbox toggles grid snaps. When grid snaps are
            active, drawing nodes are forced to the nearest grid
            intersection.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Display Grid</term>
        <listitem>
          <para>
            This toggles the visibility of the editing grid.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Grid Spacing</term>
        <listitem>
          <para>
            <guilabel>Grid Spacing</guilabel> adjusts the density of the
            grid. A higher setting here will space the grid further
            apart. A lower setting will make the grid spacing smaller.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
    <para>
      The tab section located below <guilabel>Grid</guilabel> settings
      controls the rendering settings.
    </para>
  </simplesect>

  <simplesect>
    <title>Tabs</title>
    <para>
      The first tab, <guilabel>Paint</guilabel>, contains basic brush
      options.
    </para>
    <variablelist>
      <varlistentry><term>Draw on</term>
        <listitem>
          <para>
            These options control the layer that will be used for the
            shape render.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Original</guibutton> will render the shapes
                onto the existing layer.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>New</guibutton>
                will force the creation of a new layer for the shape.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Multiple</guibutton> option will render each
                of the individual, unjoined shape components to a
                separate layer.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Using</term>
        <listitem>
          <para>
            This dropdown list allows the
            selection of the method by which the shape will be rendered.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Brush</guibutton> will render the shape
                using the currently selected brush. The
                <guilabel>Brush</guilabel> tab contains the options that
                affect this choice.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Selection</guibutton> will render the shape
                as a selection area.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Selection+Fill</guibutton> will render the
                shape as a filled selection area.
              </para>
            </listitem>
          </itemizedlist>
          <para>
            Choosing either of the selection render methods will change
            the <guilabel>Brush</guilabel> tab to a
            <guilabel>Select</guilabel> tab. The
            <guilabel>Select</guilabel> tab controls settings that
            affect the selection render methods.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>With BG of</term>
        <listitem>
          <para>
            This dropdown is available only if the
            <guilabel>Draw on</guilabel> dropdown has either
            <guibutton>New</guibutton> or
            <guibutton>Multiple</guibutton> selected. The available
            options here change the background setting for the rendered
            shape.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                The <guibutton>Transparent</guibutton> setting renders
                the shape onto a transparent background.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Background</guibutton> uses the currently
                selected background color for the shape background.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Foreground</guibutton> uses the currently
                selected foreground color to render the the background
                for the shape.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>White</guibutton> will render the shape onto
                a white background.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Copy</guibutton> will use a copy of the
                current layer for the background.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Scale to Image</term>
        <listitem>
          <para>
            Selecting the <guilabel>Scale to Image</guilabel> toggle
            will scale the rendered shape to the size of the image where
            rendering will take place. If this option is not checked,
            then the <guilabel>Scale to Image</guilabel> slider will
            become active allowing controlled resizing of the shape at
            render time. A setting of <quote>1.00</quote> will render
            the shape at 1:1 scale.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Reverse Line</term>
        <listitem>
          <para>
            This option will render the shapes in reverse. When creating
            shapes there is at least a start node and an end node. By
            this logic, these nodes, and consequently all encapsulated
            between them, can be rendered in reverse order. Rendering
            nodes in reverse can be useful for those choosing to use a
            brush that fades out, for example.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Approx. Circles/Ellipses</term>
        <listitem>
          <para>
            This option, when activated, will create an
            <link linkend="glossary-anti-aliasing">
              anti-aliasing
            </link>
            effect when rendering the arcs of circles.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>

  <simplesect>
    <title>Brushes</title>
    <variablelist>
      <varlistentry><term>Brush Selection</term>
        <listitem>
          <para>
            The <guibutton>Brush</guibutton> dropdown allows selection
            of the brush type.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                Brush, pencil, and airbrush correspond to the
                <application>GIMP</application> equivalents that are
                found in the Toolbox.
              </para>
            </listitem>
            <listitem>
              <para>
                Pattern fills the brush area with the currently selected
                pattern. Pattern is only applicable to circles and
                ellipses if <guilabel>Approx.
                Circles/Ellipses</guilabel> is active in the
                <guilabel>Paint</guilabel> tab.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Set Brush...</term>
        <listitem>
          <para>
            The <guibutton>Set Brush...</guibutton> button can be
            clicked to set up the brush in more detail. Clicking this
            button will bring forth the brush selection dialog.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Fade Out</term>
        <listitem>
          <para>
            This option is only available if
            <guibutton>Brush</guibutton> is the selected brush type. The
            slider sets the number of pixels over which the stroke
            should be faded.  A low setting is recommended to begin
            with until more
            skill is gained judging the lengths of the strokes.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Gradient</term>
        <listitem>
          <para>
            This option is only available if
            <guibutton>Brush</guibutton> is the selected brush type. The
            gradient slider selects the number of pixels along a stroke
            to render the current gradient. For example, a setting of
            fifty would render the gradient every fifty pixels.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Pressure</term>
        <listitem>
          <para>
            This option is only available if
            <guibutton>Airbrush</guibutton> is the selected brush type.
            <guilabel>Pressure</guilabel> is a slider that sets the
            amount of ink allowed into the brush shape. A low setting
            will produce a faint brush, whereas a higher setting will
            result in a more solid one.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>

  <simplesect>
    <title>Selections</title>
    <para>
      The <guilabel>Select Tab</guilabel> is only made available if
      <guibutton>Selection</guibutton> is the active option in the
      <guilabel>Paint</guilabel> tab.
    </para>
    <variablelist>
      <varlistentry><term>Selection Type</term>
        <listitem>
          <para>
            The <guilabel>Selection Type</guilabel> can be set to one of
            four things.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Add</guibutton> will merge any existing
                selection with the shape that will be rendered. If no
                selection exists, <guilabel>Gfig</guilabel> will create
                one.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Subtract</guibutton> will take the existing
                selection area and remove the shape from that area. If
                no existing selection is present,
                <guilabel>Gfig</guilabel> will do nothing.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Replace</guibutton> will remove existing
                selections and replace them with the current shape.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Intersect</guibutton> will change the current
                selection to an intersection with the shape. Any area that
                that the shape overlaps will be kept. Any other areas will
                be discarded. If no selection exists, nothing will
                occur.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Fill Type</term>
        <listitem>
          <para>
            <guilabel>Fill Type</guilabel> is only applicable if
            <guibutton>Selection+fill</guibutton> is the active
            selection in the <guilabel>Paint</guilabel> tab. There are
            three options.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Foreground</guibutton> will fill the
                selection with the current foreground color from the
                Toolbox.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Background</guibutton> will fill the
                selection with the current background color from the
                Toolbox.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Pattern</guibutton> will fill the selection
                with the current pattern fill from the Toolbox.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Fill after</term>
        <listitem>
          <para>
            The <guilabel>Fill after</guilabel> settings control how
            selection areas are painted with a pattern. This option is
            only available if <guibutton>Selection+Fill</guibutton> is
            the active <guilabel>Paint</guilabel> option.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Each Selection</guibutton> ensures that
                selections are painted after each segment of the shape
                is rendered.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>All Selections</guibutton> fills the selected
                areas only after all shape segments have been rendered.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
<!--  <varlistentry><term>Arc as</term>
        <listitem>
          <para>
            There are two available options:
          </para>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Segment</guibutton>. WTF is this shit!??!
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Sector</guibutton>. WTF is this action??!
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry> pending discovery, will ask vidar-->
      <varlistentry><term>Antialiasing</term>
        <listitem>
          <para>
            Checking the <guilabel>Antialiasing</guilabel> option will
            turn on the anti-aliasing features of the renderer.
            Anti-aliasing can give the effect of smoothness between two
            areas of differing color.
          </para>
          <para>
            See also:
            <link linkend="glossary-anti-aliasing">
              Anti-aliasing glossary entry
            </link>
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Fill Opacity</term>
        <listitem>
          <para>
            This slider sets the level of opacity that the rendered
            shape will have. A high setting here will result in more
            opacity and a lower one in less. Opacity is defined as
            <quote>The quality or state of being opaque</quote>.
            A layer or shape with less opacity will allow color
            information from underneath it to be visible to some extent
            through it. The extent is based on the level of opacity.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Feather</term>
        <listitem>
          <para>
            The <guibutton>Feather</guibutton> toggle allows specification
            of feathering of the selection area. If
            <guibutton>Feather</guibutton> is checked, the selection
            will be feathered to the amount set by the
            <guibutton>Radius</guibutton> slider. Feathering produces a
            faded edge on a selection. This can help to smooth a layer
            or shape into a background that may not quite match the
            shape in color weight.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Radius</term>
        <listitem>
          <para>
            The <guilabel>Radius</guilabel> slider is a subset of the
            <guilabel>Feather</guilabel> option. The slider sets the
            amount of feathering that will be created against the
            selection. A high setting here will result in more
            feathering.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>

  <simplesect>
    <title>Options</title>
    <para>
      The <guilabel>Options Tab</guilabel> sets many of the
      <guilabel>Gfig</guilabel> options.
    </para>
    <variablelist>
      <varlistentry><term>Show Image</term>
        <listitem>
          <para>
            The <guibutton>Show Image</guibutton> checkbox toggles whether
            the current image or layer is shown in the main
            <guilabel>Preview</guilabel> window.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Reload Image</term>
        <listitem>
          <para>
            <guibutton>Reload Image</guibutton> reloads the active
            image or layer into the main <guilabel>Preview</guilabel>
            window. This option can be used to update the window
            contents in the event that the image has been changed or
            modified while <guilabel>Gfig</guilabel> is in use.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Grid Type</term>
        <listitem>
          <itemizedlist>
            <listitem>
              <para>
                <guibutton>Rectangular</guibutton> shows the grid as a
                standard 90<superscript>o</superscript> line grid. This
                is the default for most grid based applications.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Polar</guibutton> displays the grid as a
                polar grid. Polar grids are circular, as if looking at a
                wireframe sphere from above.
              </para>
            </listitem>
            <listitem>
              <para>
                <guibutton>Isometric</guibutton> grids are common to
                technical drawing fields.
                <guibutton>Isometric</guibutton> layouts are also
                frequently seen in reference to three dimensional work.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Grid Color</term>
        <listitem>
          <para>
            The <guilabel>Grid Color</guilabel> settings control the
            color of the displayed grid. It can be useful to change
            these settings if an image has been loaded into the main
            <guilabel>Preview</guilabel> window which might obscure the
            normal grid.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Max Undo</term>
        <listitem>
          <para id="maxundo">
            This slider adjusts the number of possible undo operations
            for the drawing. Any change made to the drawing can be reverted
            or undone by using the <guibutton>Undo</guibutton> button.
            The <guibutton>Max Undo</guibutton> slider controls the exact
            number of operations that can be undone.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Show Position</term>
        <listitem>
          <para>
            The <guibutton>Show Position</guibutton> checkbox toggles the
            display of the current X,Y co-ordinates in the
            <guilabel>Object Details</guilabel> area, underneath the
            main <guilabel>Preview</guilabel> window. The coordinates
            represent the mouse cursor position within the drawing
            window.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Hide Control Points</term>
        <listitem>
          <para><!--control points is not a guilabel-->
            This option toggles the display of the control points. The
            control points are the squares that are displayed on the
            start and end points of the shape lines.
          </para><!--not that anyone would think they are-->
        </listitem>
      </varlistentry>
      <varlistentry><term>Show Tooltips</term>
        <listitem>
          <para>
            This option toggles the display of the floating tip windows
            that are displayed if the mouse cursor is hovered over a
            button or slider for a set period of time.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>About</term>
        <listitem>
          <para>
            This button displays the <guilabel>Gfig</guilabel>
            About
            dialog window. This window provides information about the
            authors.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>

  <simplesect>
    <title>Operations</title>
    <para>
      The <guilabel>Ops</guilabel> panel is located on the far left side
      of the <guilabel>Gfig</guilabel> window. This panel is used for
      the actual creation of shapes.
    </para>
    <variablelist>
      <varlistentry><term>Line Tool</term>
        <listitem>
          <para>
            The line tool is located at the top of the
            <guilabel>Ops</guilabel> panel. This is the tool used to
            draw straight lines. Lines are created by clicking and
            dragging the mouse cursor. The initial click point is the
            start of the line, and the drop point is the end of the
            line.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term> Tool</term>
        <listitem>
          <para>
            The circle tool is the second tool in the
            <guilabel>Ops</guilabel> panel. The circle tool is used to
            create perfect circles. Circles are created by clicking and
            dragging the mouse cursor. The initial click point becomes
            the center of the circle and the drop point sets the radius
            of the circle.
          </para>
        </listitem
      </varlistentry>
      <varlistentry><term>Ellipse Tool</term>
        <listitem>
          <para>
            The ellipse tool is used to create non-symmetrical circles.
            The ellipses have either a horizontal or a vertical
            alignment. Ellipses are created by dragging the mouse
            cursor. The initial click point becomes the center of the
            ellipse and the drop point sets the X and Y extremities of
            the ellipse.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Curve Tool</term>
        <listitem>
          <para>
            The curve tool draws part of a circle. To create a curve,
            <mousebutton>click</mousebutton> the start point of the arc,
            <mousebutton>click</mousebutton> the radius of the arc, and 
            finally <mousebutton>click</mousebutton> the endpoint of the
            arc.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Polygon Tool</term>
        <listitem>
          <para>
            The polygon tool draws equilateral polygons between three
            sided and two hundred sided. To set the number of sides,
            <mousebutton>double click</mousebutton> the tool button. To
            create the polygon, <mousebutton>click</mousebutton> and
            drag a line. The initial <mousebutton>click</mousebutton>
            point becomes the center of the polygon, and the drop point
            becomes the radius.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Star Tool</term>
        <listitem>
          <para>
            The star tool creates star shapes between three points and
            two hundred points. To set the number of points simply
            <mousebutton>double click</mousebutton> the tool button. To
            create the star shape, <mousebutton>click</mousebutton> and
            drag the mouse cursor. The initial
            <mousebutton>click</mousebutton> sets the center of the
            star, and the drop point sets the maximum radius.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Spiral Tool</term>
        <listitem>
          <para>
            The spiral tool, like the Polygon and Star tools, can be
            double clicked. This will display the tool options
            associated with the spiral tool. Both the direction of the
            spiral and the number of turns can be set. The creation of a
            spiral is similar to that of a circle. Simply click and drag
            a line to set the center and the radius and endpoint of the
            spiral.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Bezier Tool</term>
        <listitem>
          <para>
            The bezier tool can create abstract curves. The tool can be
            double clicked to display the tool specific options.
            Within those options are two settings.
          </para>
            <itemizedlist>
              <listitem>
                <para>
                  <guilabel>Closed</guilabel> toggles whether to close
                  the bezier curve upon completion.
                </para>
              </listitem>
              <listitem>
                <para>
                  <guilabel>Show Line Frame</guilabel> toggles the
                  display of the control lines between nodes during
                  curve creation.
                </para>
              </listitem>
            </itemizedlist>
            <para>
              Creation of a bezier curve requires at least three
              <mousebutton>clicks</mousebutton>. The first click sets
              the start point. All consecutive
              <mousebutton>clicks</mousebutton> set the controls for the
              curve. To complete the curve,
              <keycombo>
                <keycap>shift</keycap>
                <mousebutton>leftclick</mousebutton>
              </keycombo>.
              If <guilabel>Closed</guilabel> has been toggled, the curve
              will close automatically.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Move Tool</term>
        <listitem>
          <para>
            This tool allows the movement of a shape. In order to move a
            shape, <mousebutton>click</mousebutton> one of the control
            points that belong to the image and drag it. If
            <guilabel>Hide Control Points</guilabel> is toggled, the
            control points will still be able to be clicked for
            movement, but will not be visible. This will make it rather
            difficult to move the shape.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Move Single Point</term>
        <listitem>
          <para>
            This tool is used to move control points without moving the
            entire shape. This can be useful when changing the actual
            shape, and not just the position of the shape. Each of the
            primitives is affected differently by this tool.
            Of particular note are the following:
          </para>
          <itemizedlist>
            <listitem>
              <para>
                Circles and Ellipses can be moved with the center
                control point. This will however relocate the center
                control point, but not the radius control point.
              </para>
            </listitem>
            <listitem>
              <para>
                The Star shape, when created, contains a third control
                point located between the center and the radius. This
                point controls the length of the spokes.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      <varlistentry><term>Copy Tool</term>
        <listitem>
          <para>
            The copy tool is used to copy shapes.
            <mousebutton>Click</mousebutton> any control point that
            belongs to the shape that is to be moved and drag the point
            to a new location.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Delete Tool</term>
        <listitem>
          <para>
            This tool deletes shapes.
            <mousebutton>Clicking</mousebutton> on a control point will
            remove the shape that owns the control point.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>
          <quote><</quote>
            , 
          <quote>></quote>
            , and
          <quote>==</quote>
        </term>
        <listitem>
          <para>
            These three tools control what shapes are displayed in the
            main <guilabel>Preview</guilabel> window.
          </para>
          <itemizedlist>
            <listitem>
              <para>
                The <quote><</quote> and <quote>></quote> tools cycle
                through each shape individually. These tools can be
                useful for painting shapes by themselves.
              </para>
            </listitem>
            <listitem>
              <para>
                The <quote>==</quote> button redisplays all the shapes
                that may be hidden after using the <quote><</quote> and
                <quote>></quote> tools.
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
  <simplesect>
    <title>Rendering the figure</title>
    <para>
      The list of buttons located at the bottom of the
      <guilabel>Gfig</guilabel> dialog window produce, clear, and undo
      the work in the main <guilabel>Preview</guilabel> window.
    </para>
    <variablelist>
      <varlistentry><term>Done</term>
        <listitem>
          <para>
            <mousebutton>Clicking</mousebutton> will end the current
            <guilabel>Gfig</guilabel> session.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Paint</term>
        <listitem>
          <para>
            <guibutton>Paint</guibutton> will render the visible shape
            with the current settings.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Save</term>
        <listitem>
          <para>
            This button saves the current shape. If the shape has never
            been saved to disk before, <guilabel>Gfig</guilabel> will
            ask for a filename and location.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Clear</term>
        <listitem>
          <para>
            This will clear the active <guilabel>Preview</guilabel> and
            any changes will be lost.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Undo</term>
        <listitem>
          <para>
            As discussed <link linkend="maxundo">earlier</link>, this
            button will revoke the last change made to the shape or
            drawing.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry><term>Cancel</term>
        <listitem>
          <para>
            This button will close <guilabel>Gfig</guilabel>.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </simplesect>
</sect1>

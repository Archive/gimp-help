#!/bin/bash

mkdir gimp
mkdir gimp/channels
mkdir gimp/dialogs
mkdir gimp/dialogs/channels
mkdir gimp/dialogs/color_selectors
mkdir gimp/dialogs/display_filters
mkdir gimp/dialogs/gradient_editor
mkdir gimp/dialogs/layers
mkdir gimp/dialogs/palette_editor
mkdir gimp/dialogs/paths
mkdir gimp/dialogs/preferences
mkdir gimp/file
mkdir gimp/filters
mkdir gimp/image
mkdir gimp/image/edit
mkdir gimp/image/image
mkdir gimp/image/image/colors
mkdir gimp/image/image/colors/auto
mkdir gimp/image/image/mode
mkdir gimp/image/image/transforms
mkdir gimp/image/select
mkdir gimp/image/view
mkdir gimp/layers
mkdir gimp/layers/stack
mkdir gimp/open
mkdir gimp/paths
mkdir gimp/save
mkdir gimp/toolbox
mkdir gimp/toolbox/help
mkdir gimp/tools
mkdir gimp/images


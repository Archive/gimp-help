#!/bin/bash
mkdir pdf/images
mkdir pdf/images-C
cp stylesheet-images/*.* pdf/images  
cp image/images/*.* pdf/images 
cp paths/images/*.* pdf/images
cp layers/images/*.* pdf/images
cp layers/stack/images/*.* pdf/images
cp dialogs/images/*.* pdf/images
cp dialogs/paths/images/*.* pdf/images
cp dialogs/preferences/images/*.* pdf/images
cp dialogs/channels/images/*.* pdf/images
cp channels/images/*.* pdf/images
cp images-C/*.* pdf/images-C
cp filters/images-C/*.* pdf/images-C
cp image/images-C/*.* pdf/images-C
cp layers/images-C/*.* pdf/images-C
cp toolbox/images-C/*.* pdf/images-C

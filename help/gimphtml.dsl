<!--
  DSSSL stylesheet for the HTML output of the GIMP Documentation Project
  (C) 2000 by Daniel Egger <egger@suse.de> under the GPL.
-->

<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [

<!ENTITY docbook.dsl PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN" CDATA dsssl>

]>

<style-sheet>
  <style-specification use="docbook">
    <style-specification-body> 

      (define (chunk-element-list)
	(list (normalize "preface")
	      (normalize "chapter")
	      (normalize "appendix") 
	      (normalize "article")
	      (normalize "glossary")
	      (normalize "bibliography")
	      (normalize "legalnotice")
	      (normalize "index")
	      (normalize "colophon")
	      (normalize "setindex")
	      (normalize "reference")
	      (normalize "refentry")
	      (normalize "part")
	      (normalize "sect1") 
				;;(normalize "sect2") ;; What are we to do here?!
	      (normalize "section") 
	      (normalize "book") ;; just in case nothing else matches...
	      (normalize "set")  ;; sets are definitely chunks...
	      ))

      ;; Generate an extra file for the license
      (define %generate-legalnotice-link% #t)

      ;; Use the graphic buttons
      (define %admon-graphics-path% "stylesheet-images/")
      (define %admon-graphics% #t)

      ;; Put a navigationbar in front of each page
      (define %header-navigation% #t)
     
      ;; Generate an index data file which can be processed
      ;; with collateindex.pl
      (define html-index #t)
      (define html-index-filename "HTML.index")

      ;; Default extension for HTML output is .html
      (define %html-ext% ".html")
      
      ;; Default is black text on white background
      (define %body-attr% 
        (list
          (list "BGCOLOR" "#FFFFFF")
          (list "TEXT" "#000000")))

      ;; Verbatim output is shaded
      (define %shade-verbatim% #t)
      (define %generate-chapter-toc% #f)

      ;; Construct filenames from indentifiers
      (define use-id-as-filename #t)

      ;; We'll just use PNG for the graphics
      (define %graphic-default-extension% "png")

    </style-specification-body>
  </style-specification>
  <external-specification id="docbook" document="docbook.dsl">
</style-sheet>
